package com.kindred.dabu.domain.character.moves

import com.kindred.dabu.core.character.model.Move
import com.kindred.dabu.core.extension.TAG
import com.kindred.dabu.core.log.Log
import com.kindred.dabu.core.repository.Repository
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.BasePresenter
import com.kindred.dabu.domain.model.ItemToUiTransformer
import com.kindred.dabu.domain.model.move.MoveUi
import io.reactivex.rxkotlin.subscribeBy

class MovesPresenter(
    schedulers: RxSchedulers,
    private val repository: Repository,
    private val moveItemToUiTransformer: ItemToUiTransformer<Move, MoveUi>
) : BasePresenter<MovesContract.Event, MovesContract.Action>(schedulers), MovesContract.Presenter {

    override fun consume(action: MovesContract.Action) = when(action) {
        is MovesContract.Action.FetchMoves -> fetchMoves()
    }

    private fun fetchMoves() {
        repository.moves()
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onSuccess = {
                send(MovesContract.Event.RenderMoves(
                    it.map { move ->
                        moveItemToUiTransformer.transform(move)
                    }
                ))
            },
                onError = { Log.e(TAG, "Failure to fetch moves", it) }
            ).let(::addDisposable)
    }
}
