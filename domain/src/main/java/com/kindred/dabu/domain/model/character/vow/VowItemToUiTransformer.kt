package com.kindred.dabu.domain.model.character.vow

import com.kindred.dabu.core.character.model.Vow
import com.kindred.dabu.domain.model.ItemToUiTransformer
import com.kindred.dabu.domain.model.character.VowUi

class VowItemToUiTransformer : ItemToUiTransformer<Vow, VowUi> {
    override fun transform(item: Vow): VowUi = VowUi(
        id = item.id,
        name = item.name,
        difficulty = item.difficulty,
        progress = item.progress
    )
}