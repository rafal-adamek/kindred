package com.kindred.dabu.domain.model

import com.kindred.dabu.core.character.model.Item

interface ItemToUiTransformer<T1 : Item, T2 : ItemUi> {
    fun transform(item: T1): T2
}