package com.kindred.dabu.domain.character.vows

import com.kindred.dabu.core.extension.TAG
import com.kindred.dabu.core.log.Log
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.BasePresenter
import com.kindred.dabu.domain.model.character.VowUi
import com.kindred.dabu.domain.provider.CharacterProvider
import io.reactivex.rxkotlin.subscribeBy

class VowsPresenter(
    schedulers: RxSchedulers,
    private val characterProvider: CharacterProvider
) : BasePresenter<VowsContract.Event, VowsContract.Action>(schedulers), VowsContract.Presenter {


    override fun consume(action: VowsContract.Action) = with(action) {
        when (this) {
            is VowsContract.Action.ObserveBonds -> observeBonds()
            is VowsContract.Action.ObserveVows -> observeVows()
            is VowsContract.Action.AddVow -> addVow(name, difficulty)
            is VowsContract.Action.UpdateVow -> updateVow(id, name, difficulty, progress)
        }
    }

    private fun updateVow(id: Int, name: String, difficulty: Int, progress: Int) {
        characterProvider
            .update { character ->
                if (character.bonds.id == id)
                    character.copy(
                        bonds = copyVowUi(character.bonds, name, difficulty, progress)
                    )
                else
                    character.copy(vows = character.vows.map {
                        if (it.id == id) copyVowUi(it, name, difficulty, progress) else it
                    })
            }
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onComplete = { Log.d(TAG, "Successfully updated vow") },
                onError = { Log.e(TAG, "Failure to update vow", it) }
            ).let(::addDisposable)
    }

    private fun copyVowUi(item: VowUi, name: String, difficulty: Int, progress: Int) =
        item.copy(name = name, difficulty = difficulty, progress = progress)

    private fun addVow(name: String, difficulty: Int) {
        characterProvider
            .update { character ->
                character.copy(
                    vows = character.vows.plus(
                        VowUi(name = name, difficulty = difficulty, progress = 0)
                    )
                )
            }
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onComplete = { Log.d(TAG, "Successfully added vow") },
                onError = { Log.e(TAG, "Failure to add vow", it) }
            ).let(::addDisposable)
    }

    private fun observeVows() {
        characterProvider.observe()
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onNext = { send(VowsContract.Event.RenderVows(it.vows)) },
                onError = { Log.e(TAG, "Failure to fetch vows", it) }
            ).let(::addDisposable)
    }

    private fun observeBonds() {
        characterProvider.observe()
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onNext = { send(VowsContract.Event.RenderBonds(it.bonds)) },
                onError = { Log.e(TAG, "Failure to fetch bonds", it) }
            ).let(::addDisposable)
    }
}
