package com.kindred.dabu.domain.character.vows

import com.kindred.dabu.domain.BaseContract
import com.kindred.dabu.domain.model.character.VowUi

class VowsContract {
    interface Presenter : BaseContract.Presenter<Event, Action>

    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        object ObserveBonds : Action()
        object ObserveVows : Action()
        data class AddVow(val name: String, val difficulty: Int) : Action()
        data class UpdateVow(val id: Int, val name: String, val difficulty: Int, val progress: Int) : Action()
    }

    sealed class Event : BaseContract.Event {
        data class RenderVows(val vows: List<VowUi>) : Event()
        data class RenderBonds(val vow: VowUi) : Event()
    }
}
