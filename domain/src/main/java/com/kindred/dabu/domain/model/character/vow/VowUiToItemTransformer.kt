package com.kindred.dabu.domain.model.character.vow

import com.kindred.dabu.core.character.model.Vow
import com.kindred.dabu.domain.model.UiToItemTransformer
import com.kindred.dabu.domain.model.character.VowUi

class VowUiToItemTransformer : UiToItemTransformer<VowUi, Vow> {
    override fun transform(uiItem: VowUi): Vow =
        Vow(
            id = uiItem.id,
            name = uiItem.name,
            difficulty = uiItem.difficulty,
            progress = uiItem.progress
        )
}