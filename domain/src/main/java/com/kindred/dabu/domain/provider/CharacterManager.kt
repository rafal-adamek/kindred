package com.kindred.dabu.domain.provider

import com.kindred.dabu.core.character.model.Character
import com.kindred.dabu.core.local.LocalPreferences
import com.kindred.dabu.core.local.LocalStorage
import com.kindred.dabu.core.repository.Repository
import com.kindred.dabu.domain.model.ItemToUiTransformer
import com.kindred.dabu.domain.model.UiToItemTransformer
import com.kindred.dabu.domain.model.character.CharacterUi
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

class CharacterManager(
    private val repository: Repository,
    private val localStorage: LocalStorage,
    private val localPreferences: LocalPreferences,
    private val characterItemToUiTransformer: ItemToUiTransformer<Character, CharacterUi>,
    private val characterUiToItemTransformer: UiToItemTransformer<CharacterUi, Character>
) : CharacterProvider {

    private val characterSubject: Subject<CharacterUi> = BehaviorSubject.create()

    fun fetchAll(): Single<List<CharacterUi>> =
        repository.characters().map { it.map(characterItemToUiTransformer::transform) }
            .doOnSuccess { list ->
                with(list.firstOrNull { it.id == localPreferences.getPreviousCharacter(defValue = list[0].id) }) {
                    this?.let { switch(it).subscribe() }
                }
            }

    fun add(character: Character): Observable<CharacterUi> =
        localStorage.save(character)
            .andThen(Observable.just(characterItemToUiTransformer.transform(character)))

    override fun observe(): Observable<CharacterUi> =
        characterSubject
            .doOnNext { localPreferences.putPreviousCharacter(it.id) }
            .distinctUntilChanged()

    override fun update(updater: ((CharacterUi) -> CharacterUi)): Completable =
        observe()
            .firstOrError()
            .map(updater::invoke)
            .doOnSuccess {
                characterSubject.onNext(it)
            }
            .map { characterUiToItemTransformer.transform(it) }
            .flatMapCompletable { localStorage.save(it) }

    override fun switch(character: CharacterUi): Completable =
        Completable.fromAction { characterSubject.onNext(character) }
}
