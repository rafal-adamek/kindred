package com.kindred.dabu.domain.character.notes

import com.kindred.dabu.core.extension.TAG
import com.kindred.dabu.core.log.Log
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.BasePresenter
import com.kindred.dabu.domain.model.character.NoteUi
import com.kindred.dabu.domain.provider.CharacterProvider
import io.reactivex.rxkotlin.subscribeBy

class NotesPresenter(
    schedulers: RxSchedulers,
    private val characterProvider: CharacterProvider
) : BasePresenter<NotesContract.Event, NotesContract.Action>(schedulers), NotesContract.Presenter {


    override fun consume(action: NotesContract.Action) = with(action) {
        when (this) {
            is NotesContract.Action.ObserveNotes -> observeNotes()
            is NotesContract.Action.AddNote -> addNote(title, note)
            is NotesContract.Action.UpdateNote -> updateNote(id, title, note)
        }
    }

    private fun updateNote(id: Int, title: String, note: String) {
        characterProvider
            .update { character ->
                character.copy(notes = character.notes.map {
                    if (it.id == id) it.copy(title = title, note = note) else it
                })
            }
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onComplete = { Log.d(TAG, "Successfully updated note") },
                onError = { Log.e(TAG, "Failure to update note", it) }
            ).let(::addDisposable)
    }

    private fun addNote(title: String, note: String) {
        characterProvider
            .update { character ->
                character.copy(
                    notes = character.notes.plus(
                        NoteUi(title = title, note = note)
                    )
                )
            }
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onComplete = { Log.d(TAG, "Successfully added note") },
                onError = { Log.e(TAG, "Failure to add note", it) }
            ).let(::addDisposable)
    }

    private fun observeNotes() {
        characterProvider.observe()
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onNext = { eventSubject.onNext(NotesContract.Event.RenderNotes(it.notes)) },
                onError = { Log.e(TAG, "Failure to fetch notes", it) }
            ).let(::addDisposable)
    }
}
