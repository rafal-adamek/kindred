package com.kindred.dabu.domain.model.asset.character

import com.kindred.dabu.domain.model.ItemUi
import com.kindred.dabu.domain.model.asset.property.CustomAssetPropertyUi

data class CharacterAssetUi(
        override val id: Int,
        val customProperties: List<CustomAssetPropertyUi>? = null,
        val levels: List<Level>,
        val isTaken: Boolean = false
) : ItemUi {
    data class Level(val index: Int, val taken: Boolean = false)
}