package com.kindred.dabu.domain.model.oracle

import com.kindred.dabu.core.character.model.Oracle
import com.kindred.dabu.domain.model.UiToItemTransformer

class OracleUiToItemTransformer : UiToItemTransformer<OracleUi, Oracle> {
    override fun transform(uiItem: OracleUi): Oracle =
        Oracle(
            id = uiItem.id,
            name = uiItem.name,
            description = uiItem.description,
            values = uiItem.values.map { transformResultUiToItem(it) }
        )

    private fun transformResultUiToItem(uiItem: OracleUi.Result): Oracle.Result =
        Oracle.Result(result = uiItem.result, from = uiItem.range.first, to = uiItem.range.last)
}