package com.kindred.dabu.domain.model.oracle

import com.kindred.dabu.domain.model.ItemUi

data class OracleUi(
    override val id: Int,
    val name: String,
    val description: String,
    val values: List<Result>
) : ItemUi {

    data class Result(
        val result: String,
        val range: IntRange
    )
}
