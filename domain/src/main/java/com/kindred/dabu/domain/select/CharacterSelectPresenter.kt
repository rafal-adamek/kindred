package com.kindred.dabu.domain.select

import com.kindred.dabu.core.character.model.Character
import com.kindred.dabu.core.extension.TAG
import com.kindred.dabu.core.log.Log
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.BasePresenter
import com.kindred.dabu.domain.model.character.CharacterUi
import com.kindred.dabu.domain.provider.CharacterManager
import io.reactivex.rxkotlin.subscribeBy

class CharacterSelectPresenter(
    schedulers: RxSchedulers,
    private val characterProvider: CharacterManager
) : BasePresenter<CharacterSelectContract.Event, CharacterSelectContract.Action>(schedulers),
    CharacterSelectContract.Presenter {

    override fun consume(action: CharacterSelectContract.Action) = with(action) {
        when (this) {
            is CharacterSelectContract.Action.AddCharacter -> addCharacter(name)
            is CharacterSelectContract.Action.FetchCharacters -> fetchCharacters()
            is CharacterSelectContract.Action.Switch -> switch(character)
        }
    }

    private fun fetchCharacters() {
        characterProvider.fetchAll()
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onSuccess = { send(CharacterSelectContract.Event.RenderCharacters(it)) },
                onError = { Log.e(TAG, "Failure to fetch character", it) }
            ).let(::addDisposable)
    }

    private fun addCharacter(name: String) {
        with(Character(name = name)) {
            characterProvider.add(this)
                .subscribeOn(schedulers.background)
                .observeOn(schedulers.ui)
                .subscribeBy(onNext = { send(CharacterSelectContract.Event.RenderCharacter(it)) },
                    onError = { Log.e(TAG, "Something went wrong with saving the character", it) }
                ).let(::addDisposable)
        }
    }

    private fun switch(character: CharacterUi) {
        characterProvider.switch(character)
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy().let(::addDisposable)
    }
}
