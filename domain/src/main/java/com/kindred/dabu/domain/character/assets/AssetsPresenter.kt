package com.kindred.dabu.domain.character.assets

import com.kindred.dabu.core.extension.TAG
import com.kindred.dabu.core.log.Log
import com.kindred.dabu.core.repository.Repository
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.BasePresenter
import com.kindred.dabu.domain.provider.CharacterProvider
import io.reactivex.rxkotlin.subscribeBy

class AssetsPresenter(
    private val rxSchedulers: RxSchedulers,
    private val characterProvider: CharacterProvider,
    private val repository: Repository
) : BasePresenter<AssetsContract.Event, AssetsContract.Action>(rxSchedulers), AssetsContract.Presenter {

    override fun consume(action: AssetsContract.Action) = with(action) {
        when(this) {
            is AssetsContract.Action.FetchAssets -> fetchAssets()
        }
    }

    private fun fetchAssets() {
        repository.assets()
            .subscribeOn(rxSchedulers.background)
            .observeOn(rxSchedulers.ui)
            .subscribeBy(onSuccess = { TODO() },
                onError = { Log.e(TAG, "Failure to fetch assets", it) }
            ).let(::addDisposable)
    }
}
