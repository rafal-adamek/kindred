package com.kindred.dabu.domain.character

import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.BasePresenter

class CharacterPresenter(rxSchedulers: RxSchedulers) :
    BasePresenter<CharacterContract.Event, CharacterContract.Action>(rxSchedulers), CharacterContract.Presenter {
    override fun consume(action: CharacterContract.Action) = Unit
}
