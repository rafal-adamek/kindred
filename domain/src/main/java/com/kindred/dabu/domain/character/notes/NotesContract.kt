package com.kindred.dabu.domain.character.notes

import com.kindred.dabu.domain.BaseContract
import com.kindred.dabu.domain.model.character.NoteUi

class NotesContract {
    interface Presenter : BaseContract.Presenter<Event, Action>

    interface View : BaseContract.View

    sealed class Event : BaseContract.Event {
        data class RenderNotes(val notes: List<NoteUi>) : Event()
    }

    sealed class Action : BaseContract.Action {
        object ObserveNotes : Action()
        data class AddNote(val title: String, val note: String) : Action()
        data class UpdateNote(val id: Int, val title: String, val note: String) : Action()
    }
}
