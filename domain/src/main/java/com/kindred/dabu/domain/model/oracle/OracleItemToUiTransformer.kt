package com.kindred.dabu.domain.model.oracle

import com.kindred.dabu.core.character.model.Oracle
import com.kindred.dabu.domain.model.ItemToUiTransformer

class OracleItemToUiTransformer : ItemToUiTransformer<Oracle, OracleUi> {
    override fun transform(item: Oracle): OracleUi =
        OracleUi(
            id = item.id,
            name = item.name,
            description = item.description,
            values = item.values.map { transformResultItemToUi(it) })

    private fun transformResultItemToUi(item: Oracle.Result): OracleUi.Result =
        OracleUi.Result(result = item.result, range = IntRange(item.from, item.to))
}