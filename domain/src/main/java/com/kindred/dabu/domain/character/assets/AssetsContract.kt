package com.kindred.dabu.domain.character.assets

import com.kindred.dabu.domain.BaseContract

class AssetsContract {
    interface Presenter : BaseContract.Presenter<Event, Action>

    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        object FetchAssets : Action()
    }

    sealed class Event : BaseContract.Event
}
