package com.kindred.dabu.domain.select

import com.kindred.dabu.domain.BaseContract
import com.kindred.dabu.domain.model.character.CharacterUi

class CharacterSelectContract {
    interface Presenter : BaseContract.Presenter<Event, Action>

    interface View : BaseContract.View

    sealed class Event : BaseContract.Event {
        data class RenderCharacters(val characters: List<CharacterUi>) : Event()
        data class RenderCharacter(val character: CharacterUi) : Event()
    }

    sealed class Action : BaseContract.Action {
        object FetchCharacters : Action()
        data class AddCharacter(val name: String) : Action()
        data class Switch(val character: CharacterUi) : Action()
    }
}
