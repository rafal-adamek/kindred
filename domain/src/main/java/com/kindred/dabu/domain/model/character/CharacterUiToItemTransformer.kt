package com.kindred.dabu.domain.model.character

import com.kindred.dabu.core.character.model.*
import com.kindred.dabu.domain.model.UiToItemTransformer
import com.kindred.dabu.domain.model.asset.character.CharacterAssetUi
import com.kindred.dabu.domain.model.asset.property.CustomAssetPropertyUi

class CharacterUiToItemTransformer(
    private val vowUiToItemTransformer: UiToItemTransformer<VowUi, Vow>,
    private val noteUiToItemTransformer: UiToItemTransformer<NoteUi, Note>
) : UiToItemTransformer<CharacterUi, Character> {

    override fun transform(uiItem: CharacterUi): Character =
        Character(
            id = uiItem.id,
            name = uiItem.name,
            assets = uiItem.assets.map { transformCharacterAssetUiToItem(it) },
            attributes = uiItem.attributes.map { transformAttributeUiToItem(it) },
            bonds = vowUiToItemTransformer.transform(uiItem.bonds),
            debilities = uiItem.debilities.map { transformDebilitiesUiToItem(it) },
            experience = uiItem.experience,
            notes = uiItem.notes.map { noteUiToItemTransformer.transform(it) },
            stats = uiItem.stats.map { transformStatsUiToItem(it) },
            vows = uiItem.vows.map { vowUiToItemTransformer.transform(it) }
        )

    private fun transformCharacterAssetUiToItem(uiItem: CharacterAssetUi): CharacterAsset =
        CharacterAsset(
            id = uiItem.id,
            customProperties = uiItem.customProperties?.map { transformCustomPropertiesUiToItem(it) },
            levels = uiItem.levels.map { transformCharacterAssetLevelsUiToItem(it) },
            isTaken = uiItem.isTaken
        )

    private fun transformCharacterAssetLevelsUiToItem(uiItem: CharacterAssetUi.Level): CharacterAsset.Level =
        CharacterAsset.Level(index = uiItem.index, taken = uiItem.taken)

    private fun transformCustomPropertiesUiToItem(customProperty: CustomAssetPropertyUi): CharacterAsset.CustomProperty =
        when (customProperty) {
            is CustomAssetPropertyUi.TrackRange -> CharacterAsset.CustomProperty.TrackRange(
                trackMin = customProperty.range.first,
                trackMax = customProperty.range.last,
                value = customProperty.value
            )
            is CustomAssetPropertyUi.Selectable -> CharacterAsset.CustomProperty.Selectable(
                customProperty.options
            )
            is CustomAssetPropertyUi.Text -> CharacterAsset.CustomProperty.Text(
                label = customProperty.label,
                text = customProperty.text
            )
        }

    private fun transformAttributeUiToItem(uiItem: AttributeUi): Attribute =
        Attribute(
            type = when (uiItem.type) {
                AttributeUi.Type.EDGE -> Attribute.TYPE_EDGE
                AttributeUi.Type.IRON -> Attribute.TYPE_IRON
                AttributeUi.Type.HEART -> Attribute.TYPE_HEART
                AttributeUi.Type.WITS -> Attribute.TYPE_WITS
                AttributeUi.Type.SHADOW -> Attribute.TYPE_SHADOW
            }, value = uiItem.value, min = uiItem.range.first, max = uiItem.range.last
        )

    private fun transformDebilitiesUiToItem(uiItem: DebilityUi): Debility = Debility(
        type = when (uiItem.type) {
            DebilityUi.Type.WOUNDED -> Debility.TYPE_WOUNDED
            DebilityUi.Type.SHAKEN -> Debility.TYPE_SHAKEN
            DebilityUi.Type.UNPREPARED -> Debility.TYPE_UNPREPARED
            DebilityUi.Type.ENCUMBERED -> Debility.TYPE_ENCUMBERED
            DebilityUi.Type.MAIMED -> Debility.TYPE_MAIMED
            DebilityUi.Type.CORRUPTED -> Debility.TYPE_CORRUPTED
            DebilityUi.Type.CURSED -> Debility.TYPE_CURSED
            DebilityUi.Type.TORMENTED -> Debility.TYPE_TORMENTED
        }, isChecked = uiItem.isChecked
    )

    private fun transformStatsUiToItem(uiItem: StatUi): Stat = Stat(
        type = when (uiItem.type) {
            StatUi.Type.HEALTH -> Stat.TYPE_HEALTH
            StatUi.Type.SPIRIT -> Stat.TYPE_SPIRIT
            StatUi.Type.SUPPLY -> Stat.TYPE_SUPPLY
            StatUi.Type.MOMENTUM -> Stat.TYPE_MOMENTUM
        }, value = uiItem.value, min = uiItem.range.first, max = uiItem.range.last
    )

}