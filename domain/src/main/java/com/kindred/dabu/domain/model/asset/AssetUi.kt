package com.kindred.dabu.domain.model.asset

import com.kindred.dabu.domain.model.ItemUi
import com.kindred.dabu.domain.model.asset.property.CustomAssetPropertyUi

data class AssetUi(
        override val id: Int,
        val name: String,
        val customProperties: List<CustomAssetPropertyUi>? = null,
        val description: String,
        val levels: List<Level>,
        val isTaken: Boolean = false
) : ItemUi {
    data class Level(val index: Int, val description: String, val taken: Boolean = false)

}