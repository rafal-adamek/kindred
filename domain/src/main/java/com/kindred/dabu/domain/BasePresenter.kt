package com.kindred.dabu.domain

import com.kindred.dabu.core.rx.RxSchedulers
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

abstract class BasePresenter<E : BaseContract.Event, A : BaseContract.Action>(
    protected val schedulers: RxSchedulers
) : BaseContract.Presenter<E, A> {

    private val disposables = CompositeDisposable()

    protected fun addDisposable(disposable: Disposable) = disposables.add(disposable)

    protected val eventSubject: Subject<E> = PublishSubject.create()

    abstract fun consume(action: A)

    fun send(event: E) = eventSubject.onNext(event)

    override fun dispose() {
        disposables.clear()
    }

    override fun subscribe(): Observable<E> = eventSubject

    override fun attach(view: Observable<A>) {
        view.subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = ::consume)
            .let(::addDisposable)
    }

}
