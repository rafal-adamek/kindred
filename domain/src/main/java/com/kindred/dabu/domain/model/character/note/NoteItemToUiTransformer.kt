package com.kindred.dabu.domain.model.character.note

import com.kindred.dabu.core.character.model.Note
import com.kindred.dabu.domain.model.ItemToUiTransformer
import com.kindred.dabu.domain.model.character.NoteUi

class NoteItemToUiTransformer : ItemToUiTransformer<Note, NoteUi> {
    override fun transform(item: Note): NoteUi =
        NoteUi(id = item.id, title = item.title, note = item.note)
}