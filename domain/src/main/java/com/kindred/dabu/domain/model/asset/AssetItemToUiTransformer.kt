package com.kindred.dabu.domain.model.asset

import com.kindred.dabu.core.character.model.Asset
import com.kindred.dabu.domain.model.ItemToUiTransformer
import com.kindred.dabu.domain.model.asset.property.CustomAssetPropertyUi

class AssetItemToUiTransformer : ItemToUiTransformer<Asset, AssetUi> {
    override fun transform(item: Asset): AssetUi = AssetUi(
            id = item.id,
            name = item.name,
            customProperties = item.customProperties?.map { transformCustomPropertiesItemToUi(it) },
            levels = item.levels.mapIndexed { index, it ->
                transformLevelsItemToUi(
                        index = index,
                        description = it
                )
            },
            description = item.description
    )

    private fun transformLevelsItemToUi(index: Int, description: String): AssetUi.Level =
            AssetUi.Level(index = index, description = description)

    private fun transformCustomPropertiesItemToUi(customProperty: Asset.CustomProperty): CustomAssetPropertyUi =
            when (customProperty.type) {
                Asset.TYPE_TRACK ->
                    CustomAssetPropertyUi.TrackRange(
                            IntRange(
                                    start = customProperty.trackMin!!,
                                    endInclusive = customProperty.trackMax!!
                            ),
                            0
                    )
                Asset.TYPE_SELECTABLE -> CustomAssetPropertyUi.Selectable(customProperty.options!!.map { it to false }.toMap())
                Asset.TYPE_TEXT -> CustomAssetPropertyUi.Text(customProperty.name!!, "")
                else -> throw IllegalStateException("todo")
            }
}