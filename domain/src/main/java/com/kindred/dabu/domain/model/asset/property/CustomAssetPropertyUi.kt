package com.kindred.dabu.domain.model.asset.property

sealed class CustomAssetPropertyUi {
    data class TrackRange(val range: IntRange, val value: Int = 0) : CustomAssetPropertyUi()
    data class Text(val label: String, val text: String = "") : CustomAssetPropertyUi()
    data class Selectable(val options: Map<String, Boolean>) : CustomAssetPropertyUi()
}