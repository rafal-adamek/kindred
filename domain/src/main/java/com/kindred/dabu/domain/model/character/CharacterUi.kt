package com.kindred.dabu.domain.model.character

import com.kindred.dabu.core.character.model.Item
import com.kindred.dabu.domain.model.ItemUi
import com.kindred.dabu.domain.model.asset.character.CharacterAssetUi

data class CharacterUi(
    override val id: Int,
    val name: String,
    val experience: Int,
    val stats: List<StatUi>,
    val attributes: List<AttributeUi>,
    val assets: List<CharacterAssetUi>,
    val vows: List<VowUi>,
    val bonds: VowUi,
    val notes: List<NoteUi>,
    val debilities: List<DebilityUi>
) : ItemUi

data class VowUi(
    override val id: Int = Item.defaultId(),
    val name: String,
    val difficulty: Int,
    val progress: Int
) : ItemUi

data class NoteUi(
    override val id: Int = Item.defaultId(),
    val title: String,
    val note: String
) : ItemUi

data class DebilityUi(val type: Type, val isChecked: Boolean) {
    enum class Type {
        WOUNDED, UNPREPARED, SHAKEN, ENCUMBERED, MAIMED, CORRUPTED, CURSED, TORMENTED
    }
}

data class AttributeUi(val type: Type, val value: Int, val range: IntRange) {
    enum class Type {
        EDGE, HEART, IRON, SHADOW, WITS
    }
}

data class StatUi(val type: Type, val value: Int, val range: IntRange) {
    enum class Type {
        HEALTH, SPIRIT, SUPPLY, MOMENTUM
    }
}
