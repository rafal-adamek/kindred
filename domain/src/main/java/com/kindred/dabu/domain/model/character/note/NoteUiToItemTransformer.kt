package com.kindred.dabu.domain.model.character.note

import com.kindred.dabu.core.character.model.Note
import com.kindred.dabu.domain.model.UiToItemTransformer
import com.kindred.dabu.domain.model.character.NoteUi

class NoteUiToItemTransformer : UiToItemTransformer<NoteUi, Note> {
    override fun transform(uiItem: NoteUi): Note =
        Note(id = uiItem.id, title = uiItem.title, note = uiItem.note)
}