package com.kindred.dabu.domain.model.move

import com.kindred.dabu.core.character.model.Move
import com.kindred.dabu.domain.model.ItemToUiTransformer

class MoveItemToUiTransformer : ItemToUiTransformer<Move, MoveUi> {
    override fun transform(item: Move): MoveUi =
        MoveUi(
            id = item.id,
            type = transformMoveStringToType(item.type),
            description = item.description,
            name = item.name
        )

    private fun transformMoveStringToType(type: String): MoveUi.Type = when (type) {
        Move.TYPE_ADVENTURE -> MoveUi.Type.ADVENTURE
        Move.TYPE_RELATIONSHIP -> MoveUi.Type.RELATIONSHIP
        Move.TYPE_COMBAT -> MoveUi.Type.COMBAT
        Move.TYPE_SUFFER -> MoveUi.Type.SUFFER
        Move.TYPE_QUEST -> MoveUi.Type.QUEST
        Move.TYPE_FATE -> MoveUi.Type.FATE
        else -> throw IllegalStateException("todo")
    }
}