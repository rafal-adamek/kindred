package com.kindred.dabu.domain.character

import com.kindred.dabu.domain.BaseContract

class CharacterContract {
    interface Presenter : BaseContract.Presenter<Event, Action>

    interface View : BaseContract.View

    sealed class Action : BaseContract.Action

    sealed class Event : BaseContract.Event
}
