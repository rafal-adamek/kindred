package com.kindred.dabu.domain.model.move

import com.kindred.dabu.core.character.model.Move
import com.kindred.dabu.domain.model.UiToItemTransformer

class MoveUiToItemTransformer : UiToItemTransformer<MoveUi, Move> {

    override fun transform(uiItem: MoveUi): Move =
        Move(
            id = uiItem.id,
            name = uiItem.name,
            type = transformMoveTypeToString(uiItem.type),
            description = uiItem.description
        )


    private fun transformMoveTypeToString(type: MoveUi.Type): String = when (type) {
        MoveUi.Type.ADVENTURE -> Move.TYPE_ADVENTURE
        MoveUi.Type.RELATIONSHIP -> Move.TYPE_RELATIONSHIP
        MoveUi.Type.COMBAT -> Move.TYPE_COMBAT
        MoveUi.Type.SUFFER -> Move.TYPE_SUFFER
        MoveUi.Type.QUEST -> Move.TYPE_QUEST
        MoveUi.Type.FATE -> Move.TYPE_FATE
    }
}