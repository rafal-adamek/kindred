package com.kindred.dabu.domain.model.character

import com.kindred.dabu.core.character.model.*
import com.kindred.dabu.domain.model.ItemToUiTransformer
import com.kindred.dabu.domain.model.asset.character.CharacterAssetUi
import com.kindred.dabu.domain.model.asset.property.CustomAssetPropertyUi

class CharacterItemToUiTransformer(
        private val vowItemToUiTransformer: ItemToUiTransformer<Vow, VowUi>,
        private val noteItemToUiTransformer: ItemToUiTransformer<Note, NoteUi>
) : ItemToUiTransformer<Character, CharacterUi> {

    override fun transform(item: Character): CharacterUi =
            CharacterUi(
                    id = item.id,
                    name = item.name,
                    assets = item.assets.map { transformCharacterAssetItemToUi(it) },
                    attributes = item.attributes.map { transformAttributeItemToUi(it) },
                    bonds = vowItemToUiTransformer.transform(item.bonds),
                    debilities = item.debilities.map { transformDebilitiesItemToUi(it) },
                    experience = item.experience,
                    notes = item.notes.map { noteItemToUiTransformer.transform(it) },
                    stats = item.stats.map { transformStatsItemToUi(it) },
                    vows = item.vows.map { vowItemToUiTransformer.transform(it) }
            )

    private fun transformCharacterAssetItemToUi(item: CharacterAsset): CharacterAssetUi =
            CharacterAssetUi(
                    id = item.id,
                    customProperties = item.customProperties?.map { transformCustomPropertiesItemToUi(it) },
                    levels = item.levels.map { transformCharacterAssetLevelsItemToUi(it) },
                    isTaken = item.isTaken
            )

    private fun transformCharacterAssetLevelsItemToUi(item: CharacterAsset.Level): CharacterAssetUi.Level =
            CharacterAssetUi.Level(index = item.index, taken = item.taken)

    private fun transformCustomPropertiesItemToUi(customProperty: CharacterAsset.CustomProperty): CustomAssetPropertyUi =
            when (customProperty) {
                is CharacterAsset.CustomProperty.TrackRange ->
                    CustomAssetPropertyUi.TrackRange(
                            IntRange(
                                    start = customProperty.trackMin,
                                    endInclusive = customProperty.trackMax
                            ),
                            value = customProperty.value
                    )
                is CharacterAsset.CustomProperty.Selectable -> CustomAssetPropertyUi.Selectable(customProperty.options)
                is CharacterAsset.CustomProperty.Text -> CustomAssetPropertyUi.Text(label = customProperty.label, text = customProperty.text)
                else -> throw IllegalStateException("todo")
            }

    private fun transformAttributeItemToUi(item: Attribute): AttributeUi =
            AttributeUi(
                    type = when (item.type) {
                        Attribute.TYPE_EDGE -> AttributeUi.Type.EDGE
                        Attribute.TYPE_IRON -> AttributeUi.Type.IRON
                        Attribute.TYPE_HEART -> AttributeUi.Type.HEART
                        Attribute.TYPE_WITS -> AttributeUi.Type.WITS
                        Attribute.TYPE_SHADOW -> AttributeUi.Type.SHADOW
                        else -> throw IllegalStateException()
                    },
                    value = item.value,
                    range = IntRange(start = item.min, endInclusive = item.max)
            )

    private fun transformDebilitiesItemToUi(item: Debility): DebilityUi =
            DebilityUi(type = when (item.type) {
                Debility.TYPE_WOUNDED -> DebilityUi.Type.WOUNDED
                Debility.TYPE_SHAKEN -> DebilityUi.Type.SHAKEN
                Debility.TYPE_UNPREPARED -> DebilityUi.Type.UNPREPARED
                Debility.TYPE_ENCUMBERED -> DebilityUi.Type.ENCUMBERED
                Debility.TYPE_MAIMED -> DebilityUi.Type.MAIMED
                Debility.TYPE_CORRUPTED -> DebilityUi.Type.CORRUPTED
                Debility.TYPE_CURSED -> DebilityUi.Type.CURSED
                Debility.TYPE_TORMENTED -> DebilityUi.Type.TORMENTED
                else -> throw IllegalStateException()
            }, isChecked = item.isChecked)

    private fun transformStatsItemToUi(item: Stat): StatUi = StatUi(
            type = when (item.type) {
                Stat.TYPE_HEALTH -> StatUi.Type.HEALTH
                Stat.TYPE_SPIRIT -> StatUi.Type.SPIRIT
                Stat.TYPE_SUPPLY -> StatUi.Type.SUPPLY
                Stat.TYPE_MOMENTUM -> StatUi.Type.MOMENTUM
                else -> throw IllegalStateException()
            },
            value = item.value,
            range = IntRange(start = item.min, endInclusive = item.max)
    )
}
