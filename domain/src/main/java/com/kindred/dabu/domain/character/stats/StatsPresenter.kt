package com.kindred.dabu.domain.character.stats

import com.kindred.dabu.core.extension.TAG
import com.kindred.dabu.core.log.Log
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.BasePresenter
import com.kindred.dabu.domain.model.character.AttributeUi
import com.kindred.dabu.domain.model.character.DebilityUi
import com.kindred.dabu.domain.model.character.StatUi
import com.kindred.dabu.domain.provider.CharacterProvider
import io.reactivex.rxkotlin.subscribeBy

class StatsPresenter(
    schedulers: RxSchedulers,
    private val characterProvider: CharacterProvider
) : BasePresenter<StatsContract.Event, StatsContract.Action>(schedulers), StatsContract.Presenter {


    override fun consume(action: StatsContract.Action) = with(action) {
        when (this) {
            is StatsContract.Action.ObserveCharacter -> observeCharacter()
            is StatsContract.Action.UpdateAttribute -> updateAttribute(type, value)
            is StatsContract.Action.UpdateExperience -> updateExperience(value)
            is StatsContract.Action.UpdateName -> updateName(name)
            is StatsContract.Action.UpdateDebility -> updateDebility(type, isChecked)
            is StatsContract.Action.UpdateStat -> updateStat(type, value)
        }
    }

    private fun updateAttribute(type: AttributeUi.Type, value: Int) {
        characterProvider
            .update { character ->
                character.copy(attributes = character.attributes.map {
                    if (it.type == type) it.copy(
                        value = value
                    ) else it
                })
            }
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onComplete = { Log.d(TAG, "Successfully updated attribute") },
                onError = { Log.e(TAG, "Failure to update attribute", it) }
            ).let(::addDisposable)
    }

    private fun updateStat(type: StatUi.Type, value: Int) {
        characterProvider
            .update { character ->
                character.copy(stats = character.stats.map { if (it.type == type) it.copy(value = value) else it })
            }
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onComplete = { Log.d(TAG, "Successfully updated stat") },
                onError = { Log.e(TAG, "Failure to update stat", it) }
            ).let(::addDisposable)
    }

    private fun updateExperience(value: Int) {
        characterProvider
            .update { character -> character.copy(experience = value) }
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onComplete = { Log.d(TAG, "Successfully updated experience") },
                onError = { Log.e(TAG, "Failure to update experience", it) }
            ).let(::addDisposable)
    }

    private fun observeCharacter() {
        characterProvider.observe()
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onNext = { send(StatsContract.Event.RenderCharacter(it)) },
                onError = { Log.e(TAG, "Failure to fetch stats", it) }
            ).let(::addDisposable)
    }

    private fun updateName(name: String) {
        characterProvider
            .update { character -> character.copy(name = name) }
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onComplete = { Log.d(TAG, "Successfully updated name") },
                onError = { Log.e(TAG, "Failure to update name", it) }
            ).let(::addDisposable)
    }

    private fun updateDebility(type: DebilityUi.Type, isChecked: Boolean) {
        characterProvider
            .update { character ->
                character.copy(debilities = character.debilities.map {
                    if (it.type == type) it.copy(isChecked = isChecked) else it
                })
            }
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(
                onComplete = { Log.d(TAG, "Successfully updated debility") },
                onError = { Log.e(TAG, "Failure to update debility", it) }
            ).let(::addDisposable)
    }
}
