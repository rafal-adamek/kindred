package com.kindred.dabu.domain.model.move

import com.kindred.dabu.domain.model.ItemUi

data class MoveUi(
    override val id: Int,
    val name: String,
    val type: Type,
    val description: String
) : ItemUi {

    enum class Type {
        ADVENTURE, RELATIONSHIP, COMBAT, SUFFER, QUEST, FATE
    }
}
