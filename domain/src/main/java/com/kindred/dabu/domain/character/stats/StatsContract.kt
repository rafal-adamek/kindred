package com.kindred.dabu.domain.character.stats

import com.kindred.dabu.domain.BaseContract
import com.kindred.dabu.domain.model.character.AttributeUi
import com.kindred.dabu.domain.model.character.CharacterUi
import com.kindred.dabu.domain.model.character.DebilityUi
import com.kindred.dabu.domain.model.character.StatUi

class StatsContract {

    interface Presenter : BaseContract.Presenter<Event, Action>

    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        object ObserveCharacter : Action()
        data class UpdateName(val name: String) : Action()
        data class UpdateExperience(val value: Int) : Action()
        data class UpdateStat(val type: StatUi.Type, val value: Int) : Action()
        data class UpdateAttribute(val type: AttributeUi.Type, val value: Int) : Action()
        data class UpdateDebility(val type: DebilityUi.Type, val isChecked: Boolean) : Action()
    }

    sealed class Event : BaseContract.Event {
        data class RenderCharacter(val character: CharacterUi) : Event()
    }
}
