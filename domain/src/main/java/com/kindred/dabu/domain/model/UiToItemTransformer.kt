package com.kindred.dabu.domain.model

import com.kindred.dabu.core.character.model.Item

interface UiToItemTransformer<T1 : ItemUi, T2 : Item> {
    fun transform(uiItem: T1): T2
}