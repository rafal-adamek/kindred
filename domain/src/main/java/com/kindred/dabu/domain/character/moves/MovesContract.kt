package com.kindred.dabu.domain.character.moves

import com.kindred.dabu.domain.BaseContract
import com.kindred.dabu.domain.model.move.MoveUi

class MovesContract {
    interface Presenter : BaseContract.Presenter<Event, Action>

    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        object FetchMoves : Action()
    }

    sealed class Event : BaseContract.Event {
        data class RenderMoves(val moves: List<MoveUi>) : Event()
    }
}
