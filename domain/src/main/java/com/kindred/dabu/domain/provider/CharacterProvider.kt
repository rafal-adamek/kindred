package com.kindred.dabu.domain.provider

import com.kindred.dabu.domain.model.character.CharacterUi
import io.reactivex.Completable
import io.reactivex.Observable

interface CharacterProvider {
    fun observe(): Observable<CharacterUi>
    fun switch(character: CharacterUi): Completable
    fun update(updater: (CharacterUi) -> CharacterUi): Completable
}
