package com.kindred.dabu.kindred.widget.experience

interface ExperienceListener {
    fun updateExperience(value: Int)
}