package com.kindred.dabu.kindred.widget.attribute

import com.kindred.dabu.domain.model.character.AttributeUi

interface AttributeListener {
    fun updateAttribute(type: AttributeUi.Type, value: Int)
}