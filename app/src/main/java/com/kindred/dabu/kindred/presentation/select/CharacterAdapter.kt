package com.kindred.dabu.kindred.presentation.select

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import com.kindred.dabu.domain.model.character.CharacterUi
import com.kindred.dabu.kindred.R
import com.kindred.dabu.kindred.adapter.GenericListAdapter
import com.kindred.dabu.kindred.adapter.GenericViewHolder

class CharacterAdapter(
    itemCallback: DiffUtil.ItemCallback<CharacterUi>,
    private val characterActionListener: CharacterActionListener
) : GenericListAdapter<CharacterUi>(itemCallback, R.layout.element_character) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder =
        CharacterViewHolder(super.inflate(parent), characterActionListener)

    class CharacterViewHolder(
        view: View,
        private val characterActionListener: CharacterActionListener
    ) : GenericViewHolder<CharacterUi>(view) {

        private val name = view.findViewById<TextView>(R.id.name)

        override fun bindTo(item: CharacterUi) {
            name.text = item.name
            view.setOnClickListener { characterActionListener.onCharacterClicked(item) }
        }
    }

    interface CharacterActionListener {
        fun onCharacterClicked(character: CharacterUi)
    }
}
