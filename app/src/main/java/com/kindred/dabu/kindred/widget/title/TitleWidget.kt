package com.kindred.dabu.kindred.widget.title

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.kindred.dabu.kindred.R

class TitleWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val titleText: TextView
    var title: CharSequence = ""
        set(value) {
            field = value
            titleText.text = value
        }

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.widget_title, this, true)
        titleText = view.findViewById(R.id.title)
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.TitleWidget, 0, 0)
            title = resources.getText(
                typedArray.getResourceId(
                    R.styleable.TitleWidget_title_text,
                    R.string.title_default_text
                )
            )
            typedArray.recycle()
        }
    }
}