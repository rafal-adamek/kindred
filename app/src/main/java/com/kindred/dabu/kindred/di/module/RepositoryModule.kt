package com.kindred.dabu.kindred.di.module

import android.content.Context
import com.kindred.dabu.core.character.model.Character
import com.kindred.dabu.core.local.LocalDatastore
import com.kindred.dabu.core.local.LocalPreferences
import com.kindred.dabu.core.local.LocalStorage
import com.kindred.dabu.core.repository.KindredRepository
import com.kindred.dabu.core.repository.Repository
import com.kindred.dabu.domain.model.ItemToUiTransformer
import com.kindred.dabu.domain.model.UiToItemTransformer
import com.kindred.dabu.domain.model.character.CharacterUi
import com.kindred.dabu.domain.provider.CharacterManager
import com.kindred.dabu.domain.provider.CharacterProvider
import com.kindred.dabu.kindred.di.CharacterTransformer
import com.kindred.dabu.kindred.preferences.LocalSharedPreferences
import com.kindred.dabu.kindred.repository.FileDatastore
import com.kindred.dabu.kindred.repository.FileStorage
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [RepositoryModule.Bindings::class])
class RepositoryModule {

    @Module
    abstract class Bindings {
        @Binds
        abstract fun bindCharacterProvider(characterManager: CharacterManager): CharacterProvider
    }

    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder().build()

    @Provides
    fun provideLocalDatastore(context: Context, moshi: Moshi): LocalDatastore =
        FileDatastore(context, moshi, "")

    @Provides
    fun provideLocalStorage(context: Context, moshi: Moshi): LocalStorage =
        FileStorage(context.filesDir.path, moshi)

    @Provides
    fun provideRepository(localDatastore: LocalDatastore): Repository =
        KindredRepository(localDatastore)

    @Provides
    fun provideLocalPreferences(context: Context): LocalPreferences =
        LocalSharedPreferences(context)

    @Provides
    @Singleton
    fun provideCharacterProvider(
        repository: Repository,
        localStorage: LocalStorage,
        localPreferences: LocalPreferences,
        @CharacterTransformer characterItemToUiTransformer: ItemToUiTransformer<Character, CharacterUi>,
        @CharacterTransformer characterUiToItemTransformer: UiToItemTransformer<CharacterUi, Character>
    ) = CharacterManager(
        repository,
        localStorage,
        localPreferences,
        characterItemToUiTransformer,
        characterUiToItemTransformer
    )
}
