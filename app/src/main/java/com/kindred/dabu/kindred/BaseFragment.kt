package com.kindred.dabu.kindred

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.BaseContract
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject

abstract class BaseFragment<P : BaseContract.Presenter<E, A>, A : BaseContract.Action, E : BaseContract.Event> : Fragment(),
    BaseContract.View {

    @Inject
    lateinit var presenter: P

    @Inject
    lateinit var schedulers: RxSchedulers

    private val disposables = CompositeDisposable()

    abstract val layoutRes: Int

    protected val actionSubject: Subject<A> = PublishSubject.create()

    abstract fun consume(event: E)

    fun send(action: A) = actionSubject.onNext(action)

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        presenter.attach(actionSubject)
        presenter.subscribe()
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { if(isVisible) consume(it) })
            .let (::addDisposable)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(layoutRes, container, false)

    fun addDisposable(disposable: Disposable) = disposables.add(disposable)

    override fun onDetach() {
        presenter.dispose()
        disposables.clear()
        super.onDetach()
    }
}
