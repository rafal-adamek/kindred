package com.kindred.dabu.kindred.widget.stats

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageButton
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.kindred.dabu.domain.model.character.StatUi
import com.kindred.dabu.kindred.R

class StatsWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var uiItem: StatUi? = null
        set(value) {
            field = value
            if (value != null) {
                minValue = value.range.first
                maxValue = value.range.last
                this.value = value.value
                name = when (value.type) {
                    StatUi.Type.HEALTH -> healthName
                    StatUi.Type.SUPPLY -> supplyName
                    StatUi.Type.SPIRIT -> spiritName
                    StatUi.Type.MOMENTUM -> momentumName
                }
            }
        }
    var statsListener: StatsListener? = null

    var name: CharSequence = ""
        private set(value) {
            field = value
            nameText.text = value.toString()
        }
    var value: Int = 0
        private set(value) {
            field = when {
                value > maxValue -> maxValue
                value < minValue -> minValue
                else -> value
            }
            valueText.text = field.toString()
            valueText.background = when {
                field < 0 -> downValueBackground
                field > 0 -> upValueBackground
                else -> zeroValueBackground
            }
        }
    private val nameText: TextView
    private val valueText: TextView
    private var maxValue: Int = 0
    private var minValue: Int = 0
    private val down: ImageButton
    private val up: ImageButton
    private val healthName: String = context.getString(R.string.stat_health)
    private val spiritName: String = context.getString(R.string.stat_spirit)
    private val supplyName: String = context.getString(R.string.stat_supply)
    private val momentumName: String = context.getString(R.string.stat_momentum)
    private val upValueBackground: Drawable? = context.getDrawable(R.drawable.ic_stats_up)
    private val downValueBackground: Drawable? = context.getDrawable(R.drawable.ic_stats_down)
    private val zeroValueBackground: Drawable? = context.getDrawable(R.drawable.ic_stats_zero)

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.widget_stats, this, true)
        down = view.findViewById<ImageButton>(R.id.down)
            .apply {
                setOnClickListener {
                    statsListener?.updateStat(type = uiItem!!.type, value = --value)
                }
            }
        up = view.findViewById<ImageButton>(R.id.up)
            .apply {
                setOnClickListener {
                    statsListener?.updateStat(type = uiItem!!.type, value = ++value)
                }
            }
        valueText = view.findViewById(R.id.value)
        nameText = view.findViewById(R.id.name)

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.StatsWidget, 0, 0)
            value = typedArray.getInt(R.styleable.StatsWidget_stat_value, 0)
            maxValue = typedArray.getInt(R.styleable.StatsWidget_stat_max_value, 0)
            minValue = typedArray.getInt(R.styleable.StatsWidget_stat_min_value, 0)
            name = resources.getText(
                typedArray.getResourceId(
                    R.styleable.StatsWidget_stat_name,
                    R.string.stat_default_name
                )
            )
            typedArray.recycle()
        }
    }
}
