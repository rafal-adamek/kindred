package com.kindred.dabu.kindred.widget.difficulty

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.kindred.dabu.kindred.R
import com.kindred.dabu.kindred.widget.title.TitleWidget

class DifficultyWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val nameView: TitleWidget
    var name: String = ""
        private set(value) {
            field = value
            nameView.title = value
        }

    var difficulty: Int = 0
        set(value) {
            field = when {
                value > difficultyList.lastIndex -> difficultyList.lastIndex
                value < 0 -> 0
                else -> value
            }
            name = nameList[field]
            fillDifficulty()
        }
    private val difficultyList: List<ImageView>
    private val nameList: List<String>
    private val emptyDot: Drawable? = context.getDrawable(R.drawable.ic_empty_dot)
    private val filledDot: Drawable? = context.getDrawable(R.drawable.ic_filled_dot)

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.widget_difficulty, this, true)
        nameView = view.findViewById(R.id.difficultyName)
        difficultyList = listOf(
            view.findViewById(R.id.troublesome),
            view.findViewById(R.id.dangerous),
            view.findViewById(R.id.formidable),
            view.findViewById(R.id.extreme),
            view.findViewById(R.id.epic)
        )
        nameList = listOf(
            context.getString(R.string.troublesome),
            context.getString(R.string.dangerous),
            context.getString(R.string.formidable),
            context.getString(R.string.extreme),
            context.getString(R.string.epic)
        )
        difficultyList.forEachIndexed { index, imageView ->
            imageView.apply {
                setOnClickListener { difficulty = index }
                setOnLongClickListener { difficulty = index; true }
            }
        }
    }

    private fun fillDifficulty() {
        difficultyList.filterIndexed { index, _ -> index <= difficulty }
            .forEach { it.setImageDrawable(filledDot) }
        difficultyList.filterIndexed { index, _ -> index > difficulty }
            .forEach { it.setImageDrawable(emptyDot) }
    }
}