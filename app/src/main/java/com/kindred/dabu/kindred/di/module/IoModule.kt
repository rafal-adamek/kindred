package com.kindred.dabu.kindred.di.module

import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.kindred.rx.AndroidSchedulers
import dagger.Module
import dagger.Provides

@Module
class IoModule {
    @Provides
    fun provideRxSchedulers(): RxSchedulers = AndroidSchedulers()
}
