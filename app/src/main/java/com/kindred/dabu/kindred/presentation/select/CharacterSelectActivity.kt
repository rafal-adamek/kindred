package com.kindred.dabu.kindred.presentation.select

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.kindred.dabu.core.extension.replaceAll
import com.kindred.dabu.domain.model.character.CharacterUi
import com.kindred.dabu.domain.select.CharacterSelectContract
import com.kindred.dabu.kindred.BaseActivity
import com.kindred.dabu.kindred.CharacterActivity
import com.kindred.dabu.kindred.R
import com.kindred.dabu.kindred.adapter.GenericDiffUtilCallback
import kotlinx.android.synthetic.main.activity_character_select.*

class CharacterSelectActivity :
    BaseActivity<CharacterSelectContract.Presenter, CharacterSelectContract.Action, CharacterSelectContract.Event>(),
    CharacterAdapter.CharacterActionListener, CharacterSelectContract.View {

    private val adapter = CharacterAdapter(GenericDiffUtilCallback(), this)

    private val characterList: MutableList<CharacterUi> = mutableListOf()

    private var initialStart: Boolean = true

    private var splashDialog: Dialog? = null

    override fun consume(event: CharacterSelectContract.Event) = with(event) {
        when (this) {
            is CharacterSelectContract.Event.RenderCharacter -> render(character)
            is CharacterSelectContract.Event.RenderCharacters -> render(characters)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character_select)

        initialStart = if (savedInstanceState?.containsKey(KEY_INITIAL_START) == true) {
            savedInstanceState.getBoolean(KEY_INITIAL_START, true)
        } else {
            intent.getBooleanExtra(KEY_INITIAL_START, true)
        }

        recycler.adapter = adapter
        fab.setOnClickListener {
            showCharacterCreator()
        }
        send(CharacterSelectContract.Action.FetchCharacters)

        if (initialStart) splashDialog =
            Dialog(this, android.R.style.Theme_Black_NoTitleBar).apply {
                setContentView(R.layout.dialog_splash)
                show()
            }
    }

    private fun render(characters: List<CharacterUi>) {
        if (characters.isNotEmpty() && initialStart) {
            startCharacterActivity()
        } else {
            characterList.replaceAll(characters)
            adapter.submitList(characters)
            splashDialog?.dismiss()
        }
    }

    private fun render(character: CharacterUi) {
        characterList.add(character)
        adapter.submitList(characterList)
    }

    private fun startCharacterActivity() {
        startActivity(CharacterActivity.getIntent(this))
        finish()
    }

    override fun onCharacterClicked(character: CharacterUi) {
        send(CharacterSelectContract.Action.Switch(character))
        startCharacterActivity()
    }

    private fun showCharacterCreator() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_character_creator, null)
        val name: EditText = dialogView.findViewById(R.id.name)

        AlertDialog.Builder(this)
            .setView(dialogView)
            .setTitle(R.string.dialog_character_creator_title)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                send(CharacterSelectContract.Action.AddCharacter(name.text.toString()))
                dialog.dismiss()
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }

    override fun onDestroy() {
        splashDialog?.dismiss()
        super.onDestroy()
    }

    companion object {
        const val KEY_INITIAL_START = "key_initial_start"

        fun getIntent(
            context: Context,
            initialStart: Boolean
        ) = Intent(context, CharacterSelectActivity::class.java).apply {
            putExtra(KEY_INITIAL_START, initialStart)
        }
    }
}
