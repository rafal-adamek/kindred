package com.kindred.dabu.kindred.widget.attribute

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import com.kindred.dabu.domain.model.character.AttributeUi
import com.kindred.dabu.kindred.R

class AttributeWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyle, defStyleRes) {

    var uiItem: AttributeUi? = null
        set(value) {
            field = value
            if (value != null) {
                minValue = value.range.first
                maxValue = value.range.last
                this.value = value.value
                name = when (value.type) {
                    AttributeUi.Type.SHADOW -> shadowName
                    AttributeUi.Type.EDGE -> edgeName
                    AttributeUi.Type.HEART -> heartName
                    AttributeUi.Type.IRON -> ironName
                    AttributeUi.Type.WITS -> witsName
                }
            }
        }
    var attributeListener: AttributeListener? = null
    private val valueText: TextView
    private val nameText: TextView
    private var maxValue: Int = 0
    private var minValue: Int = 0
    var name: CharSequence = ""
        private set(value) {
            field = value
            nameText.text = value
        }
    var value: Int = 0
        private set(value) {
            field = when {
                value > maxValue -> maxValue
                value < minValue -> minValue
                else -> value
            }
            valueText.text = value.toString()
        }
    private val shadowName: String = context.getString(R.string.attribute_shadow)
    private val edgeName: String = context.getString(R.string.attribute_edge)
    private val heartName: String = context.getString(R.string.attribute_heart)
    private val ironName: String = context.getString(R.string.attribute_iron)
    private val witsName: String = context.getString(R.string.attribute_wits)

    init {

        val view = LayoutInflater.from(context).inflate(R.layout.widget_attribute, this, true)
        nameText = view.findViewById(R.id.attributeName)
        valueText = view.findViewById<TextView>(R.id.attributeValue).apply {
            setOnClickListener {
                attributeListener?.updateAttribute(type = uiItem!!.type, value = ++value)
            }
            setOnLongClickListener {
                attributeListener?.updateAttribute(type = uiItem!!.type, value = --value);true
            }
        }
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.AttributeWidget, 0, 0)
            value = typedArray.getInt(
                R.styleable.AttributeWidget_attribute_value,
                R.integer.attribute_default_value
            )
            name = resources.getText(
                typedArray.getResourceId(
                    R.styleable.AttributeWidget_attribute_name,
                    R.string.attribute_default_name
                )
            )
            typedArray.recycle()
        }
    }
}