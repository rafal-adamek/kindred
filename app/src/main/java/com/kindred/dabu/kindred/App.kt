package com.kindred.dabu.kindred

import android.app.Activity
import android.app.Application
import com.kindred.dabu.core.log.Log
import com.kindred.dabu.kindred.di.AppComponent
import com.kindred.dabu.kindred.di.DaggerAppComponent
import com.kindred.dabu.kindred.log.AndroidLog
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    private val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().appContext(this).build()
    }

    override fun onCreate() {
        Log.add(AndroidLog())
        appComponent.inject(this)
        super.onCreate()
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingActivityInjector

}