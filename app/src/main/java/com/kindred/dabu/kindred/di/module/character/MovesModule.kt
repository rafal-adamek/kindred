package com.kindred.dabu.kindred.di.module.character

import com.kindred.dabu.core.character.model.Move
import com.kindred.dabu.core.repository.Repository
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.character.moves.MovesContract
import com.kindred.dabu.domain.character.moves.MovesPresenter
import com.kindred.dabu.domain.model.ItemToUiTransformer
import com.kindred.dabu.domain.model.move.MoveItemToUiTransformer
import com.kindred.dabu.domain.model.move.MoveUi
import com.kindred.dabu.kindred.di.MovesScope
import dagger.Module
import dagger.Provides

@Module
class MovesModule {

    @Provides
    fun provideMoveItemToUiTransformer(): ItemToUiTransformer<Move, MoveUi> =
        MoveItemToUiTransformer()

    @Provides
    @MovesScope
    fun provideMovesPresenter(
        schedulers: RxSchedulers,
        repository: Repository,
        moveItemToUiTransformer: ItemToUiTransformer<Move, MoveUi>
    ): MovesContract.Presenter =
        MovesPresenter(schedulers, repository, moveItemToUiTransformer)
}
