package com.kindred.dabu.kindred.di.module.character

import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.character.notes.NotesContract
import com.kindred.dabu.domain.character.notes.NotesPresenter
import com.kindred.dabu.domain.provider.CharacterProvider
import com.kindred.dabu.kindred.di.NotesScope
import dagger.Module
import dagger.Provides

@Module
class NotesModule {
    @Provides
    @NotesScope
    fun provideNotesPresenter(
        schedulers: RxSchedulers,
        characterProvider: CharacterProvider
    ): NotesContract.Presenter = NotesPresenter(schedulers, characterProvider)
}