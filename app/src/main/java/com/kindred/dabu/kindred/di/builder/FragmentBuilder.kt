package com.kindred.dabu.kindred.di.builder

import com.kindred.dabu.kindred.di.*
import com.kindred.dabu.kindred.presentation.character.assets.AssetsFragment
import com.kindred.dabu.kindred.presentation.character.moves.MovesFragment
import com.kindred.dabu.kindred.presentation.character.notes.NotesFragment
import com.kindred.dabu.kindred.presentation.character.stats.StatsFragment
import com.kindred.dabu.kindred.presentation.character.vows.VowsFragment
import com.kindred.dabu.kindred.di.module.character.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = [StatsModule::class])
    @StatsScope
    abstract fun statsFragment(): StatsFragment

    @ContributesAndroidInjector(modules = [MovesModule::class])
    @MovesScope
    abstract fun movesFragment(): MovesFragment

    @ContributesAndroidInjector(modules = [AssetsModule::class])
    @AssetsScope
    abstract fun assetsFragment(): AssetsFragment

    @ContributesAndroidInjector(modules = [VowsModule::class])
    @VowsScope
    abstract fun vowsFragment(): VowsFragment

    @ContributesAndroidInjector(modules = [NotesModule::class])
    @NotesScope
    abstract fun notesFragment(): NotesFragment
}