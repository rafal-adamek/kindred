package com.kindred.dabu.kindred.presentation.character.assets

import android.content.Context
import android.os.Bundle
import com.kindred.dabu.domain.character.assets.AssetsContract
import com.kindred.dabu.kindred.BaseFragment
import com.kindred.dabu.kindred.R

class AssetsFragment : BaseFragment<AssetsContract.Presenter, AssetsContract.Action, AssetsContract.Event>(),
    AssetsContract.View {

    override val layoutRes: Int = R.layout.fragment_assets

    override fun consume(event: AssetsContract.Event) = with(event) {
        when(this) {
            else -> TODO()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        send(AssetsContract.Action.FetchAssets)
    }
}
