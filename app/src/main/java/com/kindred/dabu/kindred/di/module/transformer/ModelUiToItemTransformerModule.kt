package com.kindred.dabu.kindred.di.module.transformer

import com.kindred.dabu.core.character.model.Character
import com.kindred.dabu.core.character.model.Note
import com.kindred.dabu.core.character.model.Vow
import com.kindred.dabu.domain.model.UiToItemTransformer
import com.kindred.dabu.domain.model.character.CharacterUi
import com.kindred.dabu.domain.model.character.CharacterUiToItemTransformer
import com.kindred.dabu.domain.model.character.NoteUi
import com.kindred.dabu.domain.model.character.VowUi
import com.kindred.dabu.domain.model.character.note.NoteUiToItemTransformer
import com.kindred.dabu.domain.model.character.vow.VowUiToItemTransformer
import com.kindred.dabu.kindred.di.CharacterTransformer
import com.kindred.dabu.kindred.di.NoteTransformer
import com.kindred.dabu.kindred.di.VowTransformer
import dagger.Module
import dagger.Provides

@Module
class ModelUiToItemTransformerModule {

    @VowTransformer
    @Provides
    fun provideVowUiToItemTransformer(): UiToItemTransformer<VowUi, Vow> = VowUiToItemTransformer()

    @NoteTransformer
    @Provides
    fun provideNoteUiToItemTransformer(): UiToItemTransformer<NoteUi, Note> =
        NoteUiToItemTransformer()

    @CharacterTransformer
    @Provides
    fun provideCharacterUiToItemTransformer(
        @VowTransformer vowUiToItemTransformer: UiToItemTransformer<VowUi, Vow>,
        @NoteTransformer noteUiToItemTransformer: UiToItemTransformer<NoteUi, Note>
    ): UiToItemTransformer<CharacterUi, Character> =
        CharacterUiToItemTransformer(
            vowUiToItemTransformer,
            noteUiToItemTransformer
        )
}