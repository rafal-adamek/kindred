package com.kindred.dabu.kindred.presentation.character.moves

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import com.kindred.dabu.domain.model.move.MoveUi
import com.kindred.dabu.kindred.R
import com.kindred.dabu.kindred.adapter.GenericListAdapter
import com.kindred.dabu.kindred.adapter.GenericViewHolder

class MovesAdapter(
    itemCallback: DiffUtil.ItemCallback<MoveUi>,
    private val moveActionListener: MoveActionListener
) : GenericListAdapter<MoveUi>(itemCallback, R.layout.element_move) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoveViewHolder =
        MoveViewHolder(super.inflate(parent), moveActionListener)

    class MoveViewHolder(view: View, private val moveActionListener: MoveActionListener) :
        GenericViewHolder<MoveUi>(view) {
        val name: TextView = view.findViewById(R.id.name)
        val type: TextView = view.findViewById(R.id.type)

        override fun bindTo(item: MoveUi) {
            name.text = item.name
            type.text = view.context.getText(
                when (item.type) {
                    MoveUi.Type.ADVENTURE -> R.string.move_adventure
                    MoveUi.Type.RELATIONSHIP -> R.string.move_relationship
                    MoveUi.Type.COMBAT -> R.string.move_combat
                    MoveUi.Type.SUFFER -> R.string.move_suffer
                    MoveUi.Type.QUEST -> R.string.move_quest
                    MoveUi.Type.FATE -> R.string.move_fate
                }
            )
            view.setOnClickListener { moveActionListener.onMoveClicked(item) }
        }
    }

    interface MoveActionListener {
        fun onMoveClicked(move: MoveUi)
    }
}
