package com.kindred.dabu.kindred.presentation.character.moves

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.kindred.dabu.domain.character.moves.MovesContract
import com.kindred.dabu.domain.model.move.MoveUi
import com.kindred.dabu.kindred.BaseFragment
import com.kindred.dabu.kindred.R
import com.kindred.dabu.kindred.adapter.GenericDiffUtilCallback
import kotlinx.android.synthetic.main.fragment_moves.*

class MovesFragment : BaseFragment<MovesContract.Presenter, MovesContract.Action, MovesContract.Event>(), MovesContract.View,
    MovesAdapter.MoveActionListener {

    override val layoutRes: Int = R.layout.fragment_moves

    private val adapter: MovesAdapter = MovesAdapter(GenericDiffUtilCallback(), this)

    override fun consume(event: MovesContract.Event) = with(event) {
        when(this) {
            is MovesContract.Event.RenderMoves -> render(moves)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycler.adapter = adapter
        send(MovesContract.Action.FetchMoves)
    }

    private fun render(moves: List<MoveUi>) {
        adapter.submitList(moves)
    }

    override fun onMoveClicked(move: MoveUi) {
        AlertDialog.Builder(requireContext())
            .setTitle(move.name)
            .setMessage(move.description)
            .setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }
}
