package com.kindred.dabu.kindred

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.navigation.NavGraph
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.kindred.dabu.domain.character.CharacterContract
import com.kindred.dabu.kindred.presentation.select.CharacterSelectActivity
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_character.*
import javax.inject.Inject

class CharacterActivity :
    BaseActivity<CharacterContract.Presenter, CharacterContract.Action, CharacterContract.Event>(),
    HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun consume(event: CharacterContract.Event) = Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character)

        with(findNavController(R.id.navigationHost)) {
            navigationView.setupWithNavController(this)
            toolbar.setupWithNavController(this, AppBarConfiguration(getIdNavDestinationSet(this.graph)))
            AppBarConfiguration(this.graph)
        }

        setSupportActionBar(toolbar)
    }

    private fun getIdNavDestinationSet(navGraph: NavGraph)= navGraph.distinct().map { it.id }.toSet()


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.menu_character_select -> {
                startActivity(CharacterSelectActivity.getIntent(this, initialStart = false))
                true
            }

            R.id.menu_oracle -> {
                //TODO
                true
            }
            R.id.menu_dice -> {
                //TODO
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

    companion object {
        fun getIntent(context: Context): Intent = Intent(context, CharacterActivity::class.java)
    }
}
