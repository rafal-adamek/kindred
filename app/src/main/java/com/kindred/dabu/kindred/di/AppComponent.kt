package com.kindred.dabu.kindred.di

import android.content.Context
import com.kindred.dabu.kindred.App
import com.kindred.dabu.kindred.di.builder.ActivityBuilder
import com.kindred.dabu.kindred.di.builder.FragmentBuilder
import com.kindred.dabu.kindred.di.module.IoModule
import com.kindred.dabu.kindred.di.module.RepositoryModule
import com.kindred.dabu.kindred.di.module.transformer.ModelItemToUiTransformerModule
import com.kindred.dabu.kindred.di.module.transformer.ModelUiToItemTransformerModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilder::class,
        FragmentBuilder::class,
        IoModule::class,
        RepositoryModule::class,
        ModelUiToItemTransformerModule::class,
        ModelItemToUiTransformerModule::class
    ]
)
@Singleton
interface AppComponent {
    fun inject(app: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun appContext(app: Context): Builder

        fun build(): AppComponent
    }
}
