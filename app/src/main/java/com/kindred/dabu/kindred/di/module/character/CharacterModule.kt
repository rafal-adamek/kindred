package com.kindred.dabu.kindred.di.module.character

import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.character.CharacterContract
import com.kindred.dabu.domain.character.CharacterPresenter
import com.kindred.dabu.kindred.di.CharacterScope
import dagger.Module
import dagger.Provides

@Module
class CharacterModule {

    @Provides
    @CharacterScope
    fun providePresenter(rxSchedulers: RxSchedulers) : CharacterContract.Presenter = CharacterPresenter(rxSchedulers)

}
