package com.kindred.dabu.kindred.di.module.character

import com.kindred.dabu.domain.provider.CharacterProvider
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.character.vows.VowsContract
import com.kindred.dabu.domain.character.vows.VowsPresenter
import com.kindred.dabu.kindred.di.VowsScope
import dagger.Module
import dagger.Provides

@Module
class VowsModule {
    @Provides
    @VowsScope
    fun provideVowsPresenter(schedulers: RxSchedulers, characterProvider: CharacterProvider): VowsContract.Presenter = VowsPresenter(schedulers, characterProvider)
}