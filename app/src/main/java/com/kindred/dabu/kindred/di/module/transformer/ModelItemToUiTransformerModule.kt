package com.kindred.dabu.kindred.di.module.transformer

import com.kindred.dabu.core.character.model.Character
import com.kindred.dabu.core.character.model.Note
import com.kindred.dabu.core.character.model.Vow
import com.kindred.dabu.domain.model.ItemToUiTransformer
import com.kindred.dabu.domain.model.character.CharacterItemToUiTransformer
import com.kindred.dabu.domain.model.character.CharacterUi
import com.kindred.dabu.domain.model.character.NoteUi
import com.kindred.dabu.domain.model.character.VowUi
import com.kindred.dabu.domain.model.character.note.NoteItemToUiTransformer
import com.kindred.dabu.domain.model.character.vow.VowItemToUiTransformer
import com.kindred.dabu.kindred.di.CharacterTransformer
import com.kindred.dabu.kindred.di.NoteTransformer
import com.kindred.dabu.kindred.di.VowTransformer
import dagger.Module
import dagger.Provides

@Module
class ModelItemToUiTransformerModule {

    @VowTransformer
    @Provides
    fun provideVowItemToUiTransformer(): ItemToUiTransformer<Vow, VowUi> = VowItemToUiTransformer()

    @NoteTransformer
    @Provides
    fun provideNoteItemToUiTransformer(): ItemToUiTransformer<Note, NoteUi> =
        NoteItemToUiTransformer()

    @CharacterTransformer
    @Provides
    fun provideCharacterItemToUiTransformer(
        @VowTransformer vowItemToUiTransformer: ItemToUiTransformer<Vow, VowUi>,
        @NoteTransformer noteItemToUiTransformer: ItemToUiTransformer<Note, NoteUi>
    ): ItemToUiTransformer<Character, CharacterUi> =
        CharacterItemToUiTransformer(
            vowItemToUiTransformer,
            noteItemToUiTransformer
        )
}