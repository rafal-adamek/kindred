package com.kindred.dabu.kindred.presentation.character.vows

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.kindred.dabu.domain.model.character.VowUi
import com.kindred.dabu.kindred.R
import com.kindred.dabu.kindred.adapter.GenericListAdapter
import com.kindred.dabu.kindred.adapter.GenericViewHolder
import com.kindred.dabu.kindred.widget.vows.VowsListener
import com.kindred.dabu.kindred.widget.vows.VowsWidget

class VowsAdapter(
    itemCallback: DiffUtil.ItemCallback<VowUi>,
    private val vowsListener: VowsListener
) : GenericListAdapter<VowUi>(itemCallback, R.layout.element_vow) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VowViewHolder =
        VowViewHolder(super.inflate(parent), vowsListener)

    class VowViewHolder(view: View, private val listener: VowsListener) :
        GenericViewHolder<VowUi>(view) {

        private val vowsWidget: VowsWidget = view.findViewById(R.id.vows_widget)

        override fun bindTo(item: VowUi) {
            vowsWidget.apply { uiItem = item; vowsListener = listener }
        }
    }
}
