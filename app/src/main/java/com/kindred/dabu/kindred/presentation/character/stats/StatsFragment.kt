package com.kindred.dabu.kindred.presentation.character.stats

import android.os.Bundle
import android.widget.CheckBox
import com.kindred.dabu.domain.character.stats.StatsContract
import com.kindred.dabu.domain.model.character.AttributeUi
import com.kindred.dabu.domain.model.character.CharacterUi
import com.kindred.dabu.domain.model.character.DebilityUi
import com.kindred.dabu.domain.model.character.StatUi
import com.kindred.dabu.kindred.BaseFragment
import com.kindred.dabu.kindred.R
import com.kindred.dabu.kindred.widget.experience.ExperienceListener
import com.kindred.dabu.kindred.widget.stats.StatsListener
import com.kindred.dabu.kindred.widget.stats.StatsWidget
import kotlinx.android.synthetic.main.fragment_stats.*

class StatsFragment : BaseFragment<StatsContract.Presenter, StatsContract.Action, StatsContract.Event>(), StatsContract.View,
    ExperienceListener, StatsListener {
    override val layoutRes: Int = R.layout.fragment_stats

    override fun consume(event: StatsContract.Event) = with(event) {
        when(this) {
            is StatsContract.Event.RenderCharacter -> render(character)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        send(StatsContract.Action.ObserveCharacter)
        experience.experienceListener = this
    }

    private fun render(character: CharacterUi) {
        name.text = character.name
        experience.value = character.experience

        character.stats.forEach {
            setupStat(
                when (it.type) {
                    StatUi.Type.HEALTH -> statHealth
                    StatUi.Type.SPIRIT -> statSpirit
                    StatUi.Type.SUPPLY -> statSupply
                    StatUi.Type.MOMENTUM -> statMomentum
                }, it
            )
        }

        character.attributes.forEach {
            when (it.type) {
                AttributeUi.Type.EDGE -> attributeEdge.uiItem = it
                AttributeUi.Type.HEART -> attributeHeart.uiItem = it
                AttributeUi.Type.IRON -> attributeIron.uiItem = it
                AttributeUi.Type.SHADOW -> attributeShadow.uiItem = it
                AttributeUi.Type.WITS -> attributeWits.uiItem = it
            }
        }

        character.debilities.forEach {
            setupDebility(
                when (it.type) {
                    DebilityUi.Type.WOUNDED -> debilitiesWounded
                    DebilityUi.Type.SHAKEN -> debilitiesShaken
                    DebilityUi.Type.UNPREPARED -> debilitiesUnprepared
                    DebilityUi.Type.ENCUMBERED -> debilitiesEncumbered
                    DebilityUi.Type.MAIMED -> debilitiesMaimed
                    DebilityUi.Type.CORRUPTED -> debilitiesCorrupted
                    DebilityUi.Type.CURSED -> debilitiesCursed
                    DebilityUi.Type.TORMENTED -> debilitiesTormented
                }, it
            )

        }
    }

    override fun updateExperience(value: Int) {
        send(StatsContract.Action.UpdateExperience(value))
    }

    override fun updateStat(type: StatUi.Type, value: Int) {
        send(StatsContract.Action.UpdateStat(type = type, value = value))
    }

    private fun setupStat(statsWidget: StatsWidget, statUi: StatUi) {
        statsWidget.statsListener = this
        statsWidget.uiItem = statUi
    }

    private fun setupDebility(checkBox: CheckBox, debilityUi: DebilityUi) {
        checkBox.apply {
            isChecked = debilityUi.isChecked
            setOnClickListener {
                send(StatsContract.Action.UpdateDebility(type = debilityUi.type, isChecked = checkBox.isChecked))
            }
        }
    }
}
