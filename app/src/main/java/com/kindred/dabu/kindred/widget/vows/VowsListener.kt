package com.kindred.dabu.kindred.widget.vows

interface VowsListener {
    fun updateVow(id: Int, name: String, difficulty: Int, progress: Int)
}