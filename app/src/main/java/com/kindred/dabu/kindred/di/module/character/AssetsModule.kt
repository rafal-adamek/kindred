package com.kindred.dabu.kindred.di.module.character

import com.kindred.dabu.core.repository.Repository
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.character.assets.AssetsContract
import com.kindred.dabu.domain.character.assets.AssetsPresenter
import com.kindred.dabu.domain.provider.CharacterProvider
import com.kindred.dabu.kindred.di.AssetsScope
import dagger.Module
import dagger.Provides

@Module
class AssetsModule {
    @Provides
    @AssetsScope
    fun provideAssetsPresenter(
        rxSchedulers: RxSchedulers,
        characterProvider: CharacterProvider,
        repository: Repository
    ): AssetsContract.Presenter = AssetsPresenter(rxSchedulers, characterProvider, repository)
}
