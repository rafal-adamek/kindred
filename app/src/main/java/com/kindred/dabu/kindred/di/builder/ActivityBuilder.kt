package com.kindred.dabu.kindred.di.builder

import com.kindred.dabu.kindred.CharacterActivity
import com.kindred.dabu.kindred.di.CharacterScope
import com.kindred.dabu.kindred.di.module.CharacterSelectModule
import com.kindred.dabu.kindred.di.CharacterSelectScope
import com.kindred.dabu.kindred.di.module.character.CharacterModule
import com.kindred.dabu.kindred.presentation.select.CharacterSelectActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [CharacterModule::class])
    @CharacterScope
    abstract fun CharacterActivity(): CharacterActivity

    @ContributesAndroidInjector(modules = [CharacterSelectModule::class])
    @CharacterSelectScope
    abstract fun CharacterSelectActivity(): CharacterSelectActivity
}