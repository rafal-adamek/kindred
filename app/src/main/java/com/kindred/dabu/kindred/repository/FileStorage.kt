package com.kindred.dabu.kindred.repository

import com.kindred.dabu.core.character.model.Character
import com.kindred.dabu.core.local.LocalStorage
import com.squareup.moshi.Moshi
import io.reactivex.Completable
import java.io.File

class FileStorage(
    private val path: String,
    val moshi: Moshi
) : LocalStorage {
    override fun save(character: Character): Completable =
        Completable.fromAction {
            saveJson("${character.id}_${character.name}", toJson(character))
        }

    inline fun <reified T : Any> toJson(obj: T): String =
        moshi.adapter<T>(obj::class.java).toJson(obj)

    private fun saveJson(fileName: String, json: String) {
        File(path, "$fileName.json").writeText(json)
    }
}
