package com.kindred.dabu.kindred.widget.vows

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.snackbar.Snackbar
import com.kindred.dabu.domain.model.character.VowUi
import com.kindred.dabu.kindred.R
import kotlin.math.max

class VowsWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var uiItem: VowUi? = null
        set(value) {
            field = value
            if (value != null) {
                progressStep = value.progress
                name = value.name
                difficulty = value.difficulty
            }
        }
    var vowsListener: VowsListener? = null
    private val nameText: TextView
    var name: CharSequence = ""
        private set(value) {
            field = value
            nameText.text = value
        }

    var nameVisibility: Boolean = true
        private set(value) {
            field = value
            nameText.visibility = if (value) View.VISIBLE else GONE
        }
    var difficultyVisibility: Boolean = true
        set(value) {
            field = value
            difficultyList.forEach { it.visibility = if (value) View.VISIBLE else GONE }
        }

    private val difficultyList: List<ImageView>
    private val minDifficulty: Int = 0
    private val maxDifficulty: Int
    var difficulty: Int = 0
        private set(value) {
            field = when {
                value > maxDifficulty -> maxDifficulty
                value < minDifficulty -> minDifficulty
                else -> value
            }
            fillDifficulty()
        }


    private val progressList: List<ImageView>
    var progressStep: Int = 0
        private set(value) {
            field = when {
                value > 40 -> 40
                value < 0 -> 0
                else -> value
            }
            fillProgress()
        }
    private val emptyDot: Drawable? = context.getDrawable(R.drawable.ic_empty_dot)
    private val filledDot: Drawable? = context.getDrawable(R.drawable.ic_filled_dot)
    private val tickIcon: Drawable? = context.getDrawable(R.drawable.ic_tick)
    private val twoTicksIcon: Drawable? = context.getDrawable(R.drawable.ic_2_ticks)
    private val threeTicksIcon: Drawable? = context.getDrawable(R.drawable.ic_3_ticks)
    private val fourTicksIcon: Drawable? = context.getDrawable(R.drawable.ic_4_ticks)
    private val emptyProgressIcon: Drawable? = context.getDrawable(R.drawable.ic_empty_progress)

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.widget_vows, this, true)
        nameText = view.findViewById(R.id.name)

        difficultyList = listOf(
            view.findViewById(R.id.troublesome),
            view.findViewById(R.id.dangerous),
            view.findViewById(R.id.formidable),
            view.findViewById(R.id.extreme),
            view.findViewById(R.id.epic)
        )
        maxDifficulty = difficultyList.lastIndex
        difficultyList.forEach {
            with(it) {
                setOnClickListener { showIncreaseDifficultyDialog() }
                setOnLongClickListener { showReduceDifficultyDialog(); true }
            }
        }

        progressList = listOf(
            view.findViewById(R.id.first),
            view.findViewById(R.id.second),
            view.findViewById(R.id.third),
            view.findViewById(R.id.forth),
            view.findViewById(R.id.fifth),
            view.findViewById(R.id.sixth),
            view.findViewById(R.id.seventh),
            view.findViewById(R.id.eight),
            view.findViewById(R.id.ninth),
            view.findViewById(R.id.tenth)
        )
        progressList.forEach {
            with(it) {
                setOnClickListener { addProgress(); update(progress = progressStep) }
                setOnLongClickListener { undoProgress(); update(progress = progressStep); true }
            }
        }


        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.VowsWidget, 0, 0)
            name = resources.getText(
                typedArray.getResourceId(
                    R.styleable.VowsWidget_vow_name,
                    R.string.vows_default_name
                )
            )
            difficultyVisibility =
                typedArray.getBoolean(R.styleable.VowsWidget_vow_difficulty_visibility, true)
            nameVisibility = typedArray.getBoolean(R.styleable.VowsWidget_vow_name_visibility, true)

            typedArray.recycle()
        }
    }

    private fun showDifficultyChangedDialog(message: String, positiveAction: () -> Unit) {
        AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                positiveAction()
                dialog.dismiss()
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }

    private fun showReduceDifficultyDialog() {
        if (difficulty == minDifficulty) {
            showExtremeDifficultyDialog(context.getString(R.string.min_difficulty_message))
        } else {
            showDifficultyChangedDialog(
                context.getString(R.string.difficulty_reduce_message).plus(" ")
                    .plus(getDifficultyName(difficulty - 1))
                    .plus("?")
            ) { undoDifficulty() }
        }
    }

    private fun showIncreaseDifficultyDialog() {
        if (difficulty == maxDifficulty) {
            showExtremeDifficultyDialog(context.getString(R.string.max_difficulty_message))
        } else {
            showDifficultyChangedDialog(
                context.getString(R.string.difficulty_increase_message).plus(" ")
                    .plus(getDifficultyName(difficulty + 1))
                    .plus("?")
            ) { addDifficulty() }
        }
    }

    private fun showExtremeDifficultyDialog(message: String) {
        Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
    }

    private fun undoDifficulty() {
        --difficulty
        update(difficulty = difficulty)
    }

    private fun addDifficulty() {
        ++difficulty
        progressStep = 4
        update(difficulty = difficulty, progress = progressStep)
    }

    private fun fillDifficulty() {
        difficultyList.filterIndexed { index, _ -> index <= difficulty }
            .forEach { it.setImageDrawable(filledDot) }
        difficultyList.filterIndexed { index, _ -> index > difficulty }
            .forEach { it.setImageDrawable(emptyDot) }
    }

    private fun difficultyToStep() = when (difficulty) {
        0 -> 12
        1 -> 8
        2 -> 4
        3 -> 2
        else -> 1
    }

    private fun getDifficultyName(difficulty: Int) = when (difficulty) {
        0 -> context.getString(R.string.troublesome)
        1 -> context.getString(R.string.dangerous)
        2 -> context.getString(R.string.formidable)
        3 -> context.getString(R.string.extreme)
        else -> context.getString(R.string.epic)
    }

    private fun addProgress() {
        progressStep += difficultyToStep()
    }

    private fun undoProgress() {
        progressStep -= difficultyToStep()
    }

    private fun update(
        id: Int = uiItem!!.id,
        name: String = this.name.toString(),
        difficulty: Int = this.difficulty,
        progress: Int = progressStep
    ) {
        vowsListener?.updateVow(id = id, name = name, difficulty = difficulty, progress = progress)
    }

    private fun fillProgress() {
        progressList.forEachIndexed { index, imageView ->
            imageView.setImageDrawable(
                when (max(progressStep - (index * 4), 0)) {
                    0 -> emptyProgressIcon
                    1 -> tickIcon
                    2 -> twoTicksIcon
                    3 -> threeTicksIcon
                    else -> fourTicksIcon
                }
            )
        }
    }
}