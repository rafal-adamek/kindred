package com.kindred.dabu.kindred.di.module

import com.kindred.dabu.core.character.model.Character
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.model.ItemToUiTransformer
import com.kindred.dabu.domain.model.character.CharacterItemToUiTransformer
import com.kindred.dabu.domain.model.character.CharacterUi
import com.kindred.dabu.domain.provider.CharacterManager
import com.kindred.dabu.domain.select.CharacterSelectContract
import com.kindred.dabu.domain.select.CharacterSelectPresenter
import dagger.Module
import dagger.Provides

@Module
class CharacterSelectModule {
    @Provides
    fun provideCharacterSelectPresenter(
        rxSchedulers: RxSchedulers,
        characterManager: CharacterManager
    ): CharacterSelectContract.Presenter = CharacterSelectPresenter(rxSchedulers, characterManager)
}
