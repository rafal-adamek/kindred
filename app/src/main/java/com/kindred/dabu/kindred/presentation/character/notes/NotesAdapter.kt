package com.kindred.dabu.kindred.presentation.character.notes

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import com.kindred.dabu.domain.model.character.NoteUi
import com.kindred.dabu.kindred.R
import com.kindred.dabu.kindred.adapter.GenericListAdapter
import com.kindred.dabu.kindred.adapter.GenericViewHolder

class NotesAdapter(
    itemCallback: DiffUtil.ItemCallback<NoteUi>,
    private val noteActionListener: NoteActionListener
) : GenericListAdapter<NoteUi>(itemCallback, R.layout.element_notes) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder =
        NoteViewHolder(super.inflate(parent), noteActionListener)

    class NoteViewHolder(view: View, private val noteActionListener: NoteActionListener) :
        GenericViewHolder<NoteUi>(view) {
        private val title: TextView = view.findViewById(R.id.title)
        private val note: TextView = view.findViewById(R.id.note)


        override fun bindTo(item: NoteUi) {
            title.text = item.title
            this.note.text = item.note
            view.setOnClickListener { noteActionListener.onNoteClick(item) }
        }
    }

    interface NoteActionListener {
        fun onNoteClick(noteUi: NoteUi)
    }
}
