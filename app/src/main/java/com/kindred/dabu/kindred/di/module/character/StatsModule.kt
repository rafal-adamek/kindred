package com.kindred.dabu.kindred.di.module.character

import com.kindred.dabu.domain.provider.CharacterProvider
import com.kindred.dabu.core.rx.RxSchedulers
import com.kindred.dabu.domain.character.stats.StatsContract
import com.kindred.dabu.domain.character.stats.StatsPresenter
import dagger.Module
import dagger.Provides

@Module
class StatsModule {
    @Provides
    fun provideStatsPresenter(
        rxSchedulers: RxSchedulers,
        characterProvider: CharacterProvider
    ): StatsContract.Presenter = StatsPresenter(rxSchedulers, characterProvider)
}
