package com.kindred.dabu.kindred.repository

import android.content.Context
import com.kindred.dabu.core.character.model.Asset
import com.kindred.dabu.core.character.model.Attribute
import com.kindred.dabu.core.character.model.Character
import com.kindred.dabu.core.character.model.Move
import com.kindred.dabu.core.character.model.Stat
import com.kindred.dabu.core.local.LocalDatastore
import com.kindred.dabu.kindred.R
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.reactivex.Single
import java.io.File

class FileDatastore(
    private val context: Context,
    private val moshi: Moshi,
    private val path: String
) : LocalDatastore {

    override fun stats(): Single<List<Stat>> = Single.just(jsonToList(loadJson(R.raw.statistics)))

    override fun moves(): Single<List<Move>> = Single.just(jsonToList(loadJson(R.raw.moves)))

    override fun assets(): Single<List<Asset>> = Single.just(jsonToList(loadJson(R.raw.assets)))

    override fun attributes(): Single<List<Attribute>> = Single.just(jsonToList(loadJson(R.raw.attributes)))

    override fun characters(): Single<List<Character>> = Single.create { emitter ->
        val list = context.filesDir.walk()
            .onFail { _, ioException -> emitter.onError(ioException) }
            .filter { it.isFile }
            .filter { it.name.contains(".json") }
            .mapNotNull { jsonToObject<Character>(loadJson(it.name)) }
            .toList()

        emitter.onSuccess(list)
    }

    private fun loadJson(resourceId: Int): String {
        val stream = context.resources.openRawResource(resourceId)
        return stream.bufferedReader().use { it.readText() }
    }

    private fun loadJson(fileName: String): String =
        File(context.filesDir, fileName).inputStream().bufferedReader().use { it.readText() }

    private inline fun <reified T : Any> jsonToObject(json: String): T? =
        moshi.adapter(T::class.java).fromJson(json)

    private inline fun <reified T : Any> jsonToList(json: String): List<T>? {
        val adapterType = Types.newParameterizedType(List::class.java, T::class.java)
        return moshi.adapter<List<T>>(adapterType).fromJson(json)
    }
}
