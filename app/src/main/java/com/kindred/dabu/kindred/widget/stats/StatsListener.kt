package com.kindred.dabu.kindred.widget.stats

import com.kindred.dabu.domain.model.character.StatUi

interface StatsListener {
    fun updateStat(type: StatUi.Type, value: Int)
}