package com.kindred.dabu.kindred.presentation.character.notes

import android.content.Context
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.kindred.dabu.domain.character.notes.NotesContract
import com.kindred.dabu.domain.model.character.NoteUi
import com.kindred.dabu.kindred.BaseFragment
import com.kindred.dabu.kindred.R
import com.kindred.dabu.kindred.adapter.GenericDiffUtilCallback
import kotlinx.android.synthetic.main.fragment_moves.recycler
import kotlinx.android.synthetic.main.fragment_notes.*

class NotesFragment : BaseFragment<NotesContract.Presenter, NotesContract.Action, NotesContract.Event>(), NotesContract.View,
    NotesAdapter.NoteActionListener {

    override val layoutRes: Int = R.layout.fragment_notes

    private val adapter: NotesAdapter = NotesAdapter(GenericDiffUtilCallback(), this)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.attach(actionSubject)
    }

    override fun consume(event: NotesContract.Event) = when(event) {
        is NotesContract.Event.RenderNotes -> render(event.notes)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycler.adapter = adapter
        send(NotesContract.Action.ObserveNotes)
        fabAdd.setOnClickListener {
            showNoteDialog(
                dialogTitle = getString(R.string.add_note), title = "", content = ""
            ) { title, note -> send(NotesContract.Action.AddNote(title = title, note = note)) }
        }
    }

    private fun render(notes: List<NoteUi>) {
        adapter.submitList(notes)
    }

    override fun onNoteClick(noteUi: NoteUi) {
        showNoteDialog(
            dialogTitle = null, title = noteUi.title, content = noteUi.note
        ) { title, note ->
            send(NotesContract.Action.UpdateNote(id = noteUi.id, title = title, note = note))
        }
    }

    private fun showNoteDialog(
        dialogTitle: String?,
        title: String,
        content: String,
        onAccept: (title: String, content: String) -> Unit
    ) {
        val dialogView = layoutInflater.inflate(R.layout.dialog_note, null)
        val noteTitle: EditText = dialogView.findViewById<EditText>(R.id.title)
            .apply { setText(title) }
        val note: EditText = dialogView.findViewById<EditText>(R.id.note)
            .apply { setText(content) }
        AlertDialog.Builder(requireContext()).apply {
            setView(dialogView)
            if (dialogTitle != null) setTitle(dialogTitle)
            setPositiveButton(android.R.string.ok) { dialog, _ ->
                onAccept(noteTitle.text.toString(), note.text.toString())
                dialog.dismiss()
            }
            setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
            create()
            show()
        }
    }
}

