package com.kindred.dabu.kindred.di

import javax.inject.Scope

@Retention
@Scope
annotation class CharacterScope

@Retention
@Scope
annotation class CharacterSelectScope

@Retention @Scope
annotation class StatsScope

@Retention @Scope
annotation class AssetsScope

@Retention @Scope
annotation class MovesScope

@Retention @Scope
annotation class VowsScope

@Retention @Scope
annotation class NotesScope
