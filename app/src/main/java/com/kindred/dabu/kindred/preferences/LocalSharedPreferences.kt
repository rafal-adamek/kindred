package com.kindred.dabu.kindred.preferences

import android.content.Context
import com.kindred.dabu.core.local.LocalPreferences

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class LocalSharedPreferences(
    context: Context
) : LocalPreferences {

    private val sharedPreferences = context.getSharedPreferences(NAME_PREFERENCES, Context.MODE_PRIVATE)

    override fun getPreviousCharacter(defValue: Int) = sharedPreferences.getInt(KEY_PREVIOUS_CHARACTER, defValue)

    override fun putPreviousCharacter(previousId: Int) {
        sharedPreferences.edit().putInt(KEY_PREVIOUS_CHARACTER, previousId).apply()
    }

    companion object {
        const val NAME_PREFERENCES = "KindredPreferences"
        const val KEY_PREVIOUS_CHARACTER = "key_previous_character"
    }
}
