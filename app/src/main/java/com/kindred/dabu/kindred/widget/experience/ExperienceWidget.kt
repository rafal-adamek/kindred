package com.kindred.dabu.kindred.widget.experience

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import com.kindred.dabu.kindred.R

class ExperienceWidget @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyle, defStyleRes) {

    private val valueText: TextView
    var value: Int = 0
        set(value) {
            field = value
            valueText.text = value.toString()
        }
    var experienceListener: ExperienceListener? = null

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.widget_experience, this, true)
        view.findViewById<LinearLayout>(R.id.experience_view).apply {
            setOnClickListener { experienceListener?.updateExperience(++value) }
            setOnLongClickListener { experienceListener?.updateExperience(--value);true }
        }
        valueText = view.findViewById(R.id.attributeValue)
        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.ExperienceWidget, 0, 0)
            value = typedArray.getInt(
                R.styleable.ExperienceWidget_experience_value,
                R.integer.experience_default_value
            )
            typedArray.recycle()
        }
    }
}