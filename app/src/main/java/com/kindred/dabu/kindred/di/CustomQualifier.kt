package com.kindred.dabu.kindred.di

import javax.inject.Qualifier

@Qualifier
annotation class VowTransformer

@Qualifier
annotation class NoteTransformer

@Qualifier
annotation class CharacterTransformer