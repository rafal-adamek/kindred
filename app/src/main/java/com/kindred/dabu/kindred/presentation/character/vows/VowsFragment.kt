package com.kindred.dabu.kindred.presentation.character.vows

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.google.android.material.textfield.TextInputEditText
import com.kindred.dabu.domain.character.vows.VowsContract
import com.kindred.dabu.domain.model.character.VowUi
import com.kindred.dabu.kindred.BaseFragment
import com.kindred.dabu.kindred.R
import com.kindred.dabu.kindred.adapter.GenericDiffUtilCallback
import com.kindred.dabu.kindred.widget.difficulty.DifficultyWidget
import com.kindred.dabu.kindred.widget.vows.VowsListener
import kotlinx.android.synthetic.main.fragment_moves.recycler
import kotlinx.android.synthetic.main.fragment_vows.*

class VowsFragment :
    BaseFragment<VowsContract.Presenter, VowsContract.Action, VowsContract.Event>(),
    VowsContract.View, VowsListener {

    override fun consume(event: VowsContract.Event) = with(event) {
        when (this) {
            is VowsContract.Event.RenderVows -> renderVows(vows)
            is VowsContract.Event.RenderBonds -> renderBonds(vow)
        }
    }

    override val layoutRes: Int = R.layout.fragment_vows

    private val adapter = VowsAdapter(GenericDiffUtilCallback(), this)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycler.adapter = adapter
        send(VowsContract.Action.ObserveBonds)
        send(VowsContract.Action.ObserveVows)
        addVowClick()
    }

    private fun renderBonds(vow: VowUi) {
        bonds.uiItem = vow
        bonds.vowsListener = this
    }

    private fun renderVows(vows: List<VowUi>) {
        adapter.submitList(vows)
    }

    override fun updateVow(id: Int, name: String, difficulty: Int, progress: Int) {
        send(
            VowsContract.Action.UpdateVow(
                id = id,
                name = name,
                difficulty = difficulty,
                progress = progress
            )
        )
    }

    private fun addVowClick() {
        fabAdd.setOnClickListener {
            val dialogView = layoutInflater.inflate(R.layout.dialog_add_vow, null)
            val name: TextInputEditText = dialogView.findViewById(R.id.name_input)
            val difficultyWidget: DifficultyWidget = dialogView.findViewById(R.id.difficulty)
            AlertDialog.Builder(requireContext())
                .setIcon(R.drawable.ic_vows)
                .setView(dialogView)
                .setTitle(getString(R.string.add_vow))
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    send(
                        VowsContract.Action.AddVow(
                            name = name.text.toString(),
                            difficulty = difficultyWidget.difficulty
                        )
                    )
                    dialog.dismiss()
                }
                .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                .create()
                .show()
        }
    }
}

