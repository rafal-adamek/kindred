package com.kindred.dabu.kindred.repository

import com.kindred.dabu.core.character.model.Character
import com.squareup.moshi.Moshi
import org.junit.Test

class FileStorageTest {

    @Test(expected = Test.None::class)
    fun `Character parsed properly`() {
        val fileStorage = FileStorage("", Moshi.Builder().build())
        val character = Character(id = 0, name = "Testoviron")
        fileStorage.toJson(character)
    }
}
