package com.kindred.dabu.core.extension

val Any.TAG
    get() = this.javaClass.simpleName
