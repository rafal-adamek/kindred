package com.kindred.dabu.core.character.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Asset(
    override val id: Int,
    val name: String,
    val customProperties: List<CustomProperty>? = null,
    val description: String = "",
    val levels: List<String> = listOf()
) : Item() {

    @JsonClass(generateAdapter = true)
    data class CustomProperty(
        val type: String? = null,
        val name: String? = null,
        @Json(name = "track_max") val trackMin: Int? = null,
        @Json(name = "track_min") val trackMax: Int? = null,
        @Json(name = "default_levels") val defaultLevels: List<Int>? = null,
        val options: List<String>? = null
    )

    companion object {
        const val TYPE_TEXT = "Text"
        const val TYPE_TRACK = "Track"
        const val TYPE_SELECTABLE = "Selectable"
        const val TYPE_DEFAULT = "Default"
    }

}
