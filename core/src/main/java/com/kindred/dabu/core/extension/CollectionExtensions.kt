package com.kindred.dabu.core.extension

fun <T>MutableList<T>.replaceAll(newList: List<T>) {
    clear()
    addAll(newList)
}