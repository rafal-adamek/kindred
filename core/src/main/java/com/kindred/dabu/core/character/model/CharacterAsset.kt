package com.kindred.dabu.core.character.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CharacterAsset(
        override val id: Int,
        val customProperties: List<CustomProperty>? = null,
        val levels: List<Level>,
        val isTaken: Boolean = false
) : Item() {
    data class Level(val index: Int, val taken: Boolean = false)

    @JsonClass(generateAdapter = true)
    open class CustomProperty {
        @JsonClass(generateAdapter = true)
        data class TrackRange(val trackMin: Int = 0, val trackMax: Int = 0, val value: Int = 0) : CustomProperty()
        @JsonClass(generateAdapter = true)
        data class Text(val label: String, val text: String = "") : CustomProperty()
        @JsonClass(generateAdapter = true)
        data class Selectable(val options: Map<String, Boolean>) : CustomProperty()
    }
}
