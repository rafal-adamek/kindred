package com.kindred.dabu.core.character.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Character(
    override val id: Int = defaultId(),
    val name: String,
    val experience: Int = 0,
    val stats: List<Stat> = defaultStats(),
    val attributes: List<Attribute> = defaultAttributes(),
    val assets: List<CharacterAsset> = listOf(),
    val vows: List<Vow> = listOf(),
    val bonds: Vow = Vow(name = "Bond", difficulty = 4),
    val notes: List<Note> = listOf(),
    val debilities: List<Debility> = defaultDebilities()
) : Item()

@JsonClass(generateAdapter = true)
data class Vow(
    override val id: Int = defaultId(),
    val name: String,
    val difficulty: Int = 0,
    val progress: Int = 0
) : Item(id = id)

@JsonClass(generateAdapter = true)
data class Note(
    override val id: Int = defaultId(),
    val title: String,
    val note: String
) : Item()


fun defaultStats() = listOf(
    Stat(type = Stat.TYPE_HEALTH, value = 5, min = 0, max = 5),
    Stat(type = Stat.TYPE_SPIRIT, value = 5, min = 0, max = 5),
    Stat(type = Stat.TYPE_SUPPLY, value = 5, min = 0, max = 5),
    Stat(type = Stat.TYPE_MOMENTUM, value = 0, min = -6, max = 10)
)

fun defaultAttributes() = listOf(
    Attribute(type = Attribute.TYPE_EDGE, value = 0, min = 0, max = 3),
    Attribute(type = Attribute.TYPE_IRON, value = 0, min = 0, max = 3),
    Attribute(type = Attribute.TYPE_HEART, value = 0, min = 0, max = 3),
    Attribute(type = Attribute.TYPE_SHADOW, value = 0, min = 0, max = 3),
    Attribute(type = Attribute.TYPE_WITS, value = 0, min = 0, max = 3)
)

fun defaultDebilities() = listOf(
    Debility(type = Debility.TYPE_WOUNDED, isChecked = false),
    Debility(type = Debility.TYPE_UNPREPARED, isChecked = false),
    Debility(type = Debility.TYPE_SHAKEN, isChecked = false),
    Debility(type = Debility.TYPE_ENCUMBERED, isChecked = false),
    Debility(type = Debility.TYPE_MAIMED, isChecked = false),
    Debility(type = Debility.TYPE_CORRUPTED, isChecked = false),
    Debility(type = Debility.TYPE_CURSED, isChecked = false),
    Debility(type = Debility.TYPE_TORMENTED, isChecked = false)
)
