package com.kindred.dabu.core.repository

import com.kindred.dabu.core.character.model.Asset
import com.kindred.dabu.core.character.model.Attribute
import com.kindred.dabu.core.character.model.Character
import com.kindred.dabu.core.character.model.Move
import com.kindred.dabu.core.character.model.Stat
import com.kindred.dabu.core.local.LocalDatastore
import com.squareup.moshi.Moshi
import io.reactivex.Single

class KindredRepository(private val localDatastore: LocalDatastore) : Repository {
    override fun characters(): Single<List<Character>> = localDatastore.characters()

    override fun stats(): Single<List<Stat>> = localDatastore.stats()

    override fun moves(): Single<List<Move>> = localDatastore.moves()

    override fun assets(): Single<List<Asset>> = localDatastore.assets()

    override fun attributes(): Single<List<Attribute>> = localDatastore.attributes()
}
