package com.kindred.dabu.core.local

interface LocalPreferences {
    fun getPreviousCharacter(defValue: Int = NO_ID): Int
    fun putPreviousCharacter(previousId: Int)

    companion object {
        const val NO_ID = -1
    }

}
