package com.kindred.dabu.core.character.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Move(
    override val id: Int,
    val name: String,
    val type: String,
    val description: String
) : Item() {
    companion object {
        const val TYPE_ADVENTURE = "Adventure"
        const val TYPE_RELATIONSHIP = "Relationship"
        const val TYPE_COMBAT = "Combat"
        const val TYPE_SUFFER = "Suffer"
        const val TYPE_QUEST = "Quest"
        const val TYPE_FATE = "Fate"
    }
}
