package com.kindred.dabu.core.character.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Debility(val type: Int, val isChecked: Boolean = false) {
    companion object {
        const val TYPE_WOUNDED: Int = 200
        const val TYPE_UNPREPARED: Int = 201
        const val TYPE_SHAKEN: Int = 202
        const val TYPE_ENCUMBERED: Int = 203
        const val TYPE_MAIMED: Int = 204
        const val TYPE_CORRUPTED: Int = 205
        const val TYPE_CURSED: Int = 206
        const val TYPE_TORMENTED: Int = 207
    }
}
