package com.kindred.dabu.core.character.model

import java.util.*

abstract class Item(open val id: Int = defaultId()) {
    companion object {
        fun defaultId() = UUID.randomUUID().hashCode()
    }
}
