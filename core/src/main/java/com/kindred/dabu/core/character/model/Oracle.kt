package com.kindred.dabu.core.character.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Oracle(
    override val id: Int,
    val name: String,
    val description: String,
    val values: List<Result>
) : Item() {

    @JsonClass(generateAdapter = true)
    data class Result(
        val result: String,
        val from: Int,
        val to: Int
    )
}
