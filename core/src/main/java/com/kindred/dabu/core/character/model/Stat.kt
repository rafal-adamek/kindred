package com.kindred.dabu.core.character.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Stat(val type: Int, val value: Int = 5, val min: Int = 0, val max: Int = 5) {
    companion object {
        const val TYPE_HEALTH: Int = 0
        const val TYPE_SPIRIT: Int = 1
        const val TYPE_SUPPLY: Int = 2
        const val TYPE_MOMENTUM: Int = 3
    }
}