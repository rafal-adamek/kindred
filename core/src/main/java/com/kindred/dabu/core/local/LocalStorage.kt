package com.kindred.dabu.core.local

import com.kindred.dabu.core.character.model.Character
import io.reactivex.Completable

interface LocalStorage {
    fun save(character: Character): Completable
}
