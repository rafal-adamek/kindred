package com.kindred.dabu.core.character.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Attribute(val type: Int, val value: Int = 5, val min: Int = 0, val max: Int = 5) {
    companion object {
        const val TYPE_EDGE: Int = 100
        const val TYPE_IRON: Int = 101
        const val TYPE_HEART: Int = 102
        const val TYPE_SHADOW: Int = 103
        const val TYPE_WITS: Int = 104
    }
}