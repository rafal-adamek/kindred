package com.kindred.dabu.core.repository

import com.kindred.dabu.core.character.model.Asset
import com.kindred.dabu.core.character.model.Attribute
import com.kindred.dabu.core.character.model.Character
import com.kindred.dabu.core.character.model.Move
import com.kindred.dabu.core.character.model.Stat
import io.reactivex.Single

interface Repository {
    fun characters(): Single<List<Character>>
    fun stats(): Single<List<Stat>>
    fun moves(): Single<List<Move>>
    fun assets(): Single<List<Asset>>
    fun attributes(): Single<List<Attribute>>
 }
